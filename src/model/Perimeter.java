package model;

import java.io.Serializable;

public class Perimeter implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private boolean hasChanged;
	
	private int id;
	private String name;
	private String locality;
	private int locations;
	
	public Perimeter (String name, String locality)
	{
		this(-1, name, locality);
	}
	
	public Perimeter (int id, String name, String locality)
	{
		this.id = id;
		this.name = name;
		this.locality = locality;
		this.locations = 0;
		hasChanged = false;
	}
	
	public boolean hasChanged()
	{
		return hasChanged;
	}
	
	public void setHasChanged(boolean value)
	{
		hasChanged = value;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		if (!name.equals(this.name))
		{
			this.name = name;
			hasChanged = true;
		}
	}

	public String getLocality()
	{
		return locality;
	}

	public void setLocality(String locality)
	{
		if (!locality.equals(this.locality))
		{
			this.locality = locality;
			hasChanged = true;
		}
	}

	public int getLocations()
	{
		return locations;
	}

	public void setLocations(int locations)
	{
		this.locations = locations;
	}
}

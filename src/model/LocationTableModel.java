package model;

import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class LocationTableModel extends AbstractTableModel
{
	private ArrayList<Location> rowData = new ArrayList<Location>();
	private ArrayList<String> columnNames = new ArrayList<String>();
	
	public LocationTableModel(final ArrayList<Location> locations) 
	{
		rowData = locations;
		columnNames.add("Id");
		columnNames.add("Name");
		columnNames.add("Address");
		columnNames.add("Perimeter");
		columnNames.add("Clients");
		columnNames.add("Admins");
	}
	
	public Class<?> getColumnClass(int column)
	{
		switch (column)
		{
		case 0:
			return Integer.class;
		case 1:
			return String.class;
		case 2:
			return String.class;
		case 3:
			return String.class;
		case 4:
			return Integer.class;
		case 5:
			return Integer.class;
		default:
			return String.class;
		}
	}

	public ArrayList<Location> getChangedLocations()
	{
		ArrayList<Location> changed = new ArrayList<Location>(getRowCount());

		for (Location l : rowData)
		{
			if (l.hasChanged())
			{
				changed.add(l);
			}
		}

		return changed;
	}
	
	public Location getLocationAt(int rowIndex)
	{
		return rowData.get(rowIndex);
	}
	
	public int getColumnCount() 
	{
		return columnNames.size();
	}

	public int getRowCount() 
	{
		return rowData.size();
	}

	public String getColumnName(int col) 
	{
		return columnNames.get(col);
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) 
	{
		if (columnIndex >= 1 && columnIndex <= 2)
			return true;
		
		return false;
	}

	public Object getValueAt(int rowIndex, int columnIndex) 
	{
		switch (columnIndex) 
		{
		case 0:
			return rowData.get(rowIndex).getId();
		case 1:
			return rowData.get(rowIndex).getName();
		case 2:
			return rowData.get(rowIndex).getAddress();
		case 3:
			return rowData.get(rowIndex).getPerimeterName();
		case 4:
			return rowData.get(rowIndex).getClients();
		case 5:
			return rowData.get(rowIndex).getAdmins();
		default:
			return null;
		}
	}
	
	public void setValueAt(Object value, int rowIndex, int columnIndex)
	{
		Location loc = rowData.get(rowIndex);
		switch (columnIndex)
		{
		case 1:
			if (verifyInput(value.toString(), "^[a-zA-Z0-9][a-zA-Z0-9-. ]{3,}"))
				loc.setName(value.toString());
			break;
		case 2:
			if (verifyInput(value.toString(), "^[a-zA-Z0-9][a-zA-Z0-9-,. ]{5,}"))
				loc.setAddress(value.toString());
			break;
		}
		fireTableRowsUpdated(rowIndex, rowIndex);
	}

	public ArrayList<Location> getRowData() 
	{
		return rowData;
	}

	public void setRowData(ArrayList<Location> rowData) 
	{
		this.rowData = rowData;
	}
	
	public boolean verifyInput(String input, String regex)
	{
		final Pattern pattern = Pattern.compile(regex);
		
	    if (!pattern.matcher(input).matches()) 
	    {
	    	JOptionPane.showMessageDialog(null,
				    "Invalid input! Please enter a valid input.",
				    "Error!",
				    JOptionPane.ERROR_MESSAGE);
	        return false;
	    }
	    else
	    {
	    	return true;
	    }
	}
}

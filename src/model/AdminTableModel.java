package model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class AdminTableModel extends AbstractTableModel
{
	private ArrayList<Client> rowData = new ArrayList<Client>();
	private ArrayList<String> columnNames = new ArrayList<String>();
	
	public AdminTableModel(final ArrayList<Client> clients) 
	{
		rowData = clients;
		columnNames.add("CNP");
		columnNames.add("First name");
		columnNames.add("Last name");
		columnNames.add("Birthday");
		columnNames.add("Address");
		columnNames.add("Phone");
		columnNames.add("Rank");
		columnNames.add("Location");
	}
	
	public Class<?> getColumnClass(int column)
	{
		switch (column)
		{
		case 0:
			return String.class;
		case 1:
			return String.class;
		case 2:
			return String.class;
		case 3:
			return Date.class;
		case 4:
			return String.class;
		case 5:
			return String.class;
		case 6:
			return String.class;
		case 7:
			return String.class;
		default:
			return String.class;
		}
	}

	public ArrayList<Client> getChangedClients()
	{
		ArrayList<Client> changed = new ArrayList<Client>(getRowCount());

		for (Client c : rowData)
		{
			if (c.hasChanged())
			{
				changed.add(c);
			}
		}

		return changed;
	}
	
	public Client getClientAt(int rowIndex)
	{
		return rowData.get(rowIndex);
	}
	
	public int getColumnCount() 
	{
		return columnNames.size();
	}

	public int getRowCount() 
	{
		return rowData.size();
	}

	public String getColumnName(int col) 
	{
		return columnNames.get(col);
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) 
	{
		if (columnIndex == 0 || columnIndex == 7)
			return false;
		
		return true;
	}

	public Object getValueAt(int rowIndex, int columnIndex) 
	{
		switch (columnIndex) 
		{
		case 0:
			return rowData.get(rowIndex).getCNP();
		case 1:
			return rowData.get(rowIndex).getFirstName();
		case 2:
			return rowData.get(rowIndex).getLastName();
		case 3:
			return rowData.get(rowIndex).getBirthday();
		case 4:
			return rowData.get(rowIndex).getAddress();
		case 5:
			return rowData.get(rowIndex).getPhoneNumber();
		case 6:
			return rowData.get(rowIndex).getCompanyRank();
		case 7:
			return rowData.get(rowIndex).getLocationName();
		default:
			return null;
		}
	}
	
	public void setValueAt(Object value, int rowIndex, int columnIndex)
	{
		Client client = rowData.get(rowIndex);
		switch (columnIndex)
		{
		case 1:
			if (verifyInput(value.toString(), "^[a-zA-Z][a-zA-Z- ]{3,}"))
				client.setFirstName(value.toString());
			break;
		case 2:
			if (verifyInput(value.toString(), "^[a-zA-Z][a-zA-Z- ]{3,}"))
				client.setLastName(value.toString());
			break;
		case 3:
			if (verifyInput(value.toString(), "((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])"))
				client.setBirthday((Date)value);
			break;
		case 4:
			if (verifyInput(value.toString(), "^[a-zA-Z0-9][a-zA-Z0-9-,. ]{6,}"))
				client.setAddress(value.toString());
			break;
		case 5:
			if (verifyInput(value.toString(), "\\b0{1}7{1}[2-9]{1}\\d{1}[-. ]?\\d{3}[-. ]?\\d{3}\\b"))
				client.setPhoneNumber(value.toString());
			break;
		case 6:
			if (verifyInput(value.toString(), "^[a-zA-Z][a-zA-Z- ]{2,}"))
				client.setCompanyRank(value.toString());
			break;		
		}
		fireTableRowsUpdated(rowIndex, rowIndex);
	}

	public ArrayList<Client> getRowData() 
	{
		return rowData;
	}

	public void setRowData(ArrayList<Client> rowData) 
	{
		this.rowData = rowData;
	}
	
	public boolean verifyInput(String input, String regex)
	{
		final Pattern pattern = Pattern.compile(regex);
		
	    if (!pattern.matcher(input).matches()) 
	    {
	    	JOptionPane.showMessageDialog(null,
				    "Invalid input! Please enter a valid input.",
				    "Error!",
				    JOptionPane.ERROR_MESSAGE);
	        return false;
	    }
	    else
	    {
	    	return true;
	    }
	}
}

package model;

import java.io.Serializable;
import java.sql.Date;

public class Client implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String CNP;
	private String firstName;
	private String lastName;
	private Date birthday;
	private String address;
	private boolean isAdmin;
	private String phoneNumber;
	private String companyRank;
	private int locationId;
	
	private boolean hasChanged;
	private String locationName;
	
	public Client(String cNP, String firstName, String lastName, Date birthday, 
			String address, boolean isAdmin, String phoneNumber,
			String companyRank, int locationId)
	{
		hasChanged = false;
		CNP = cNP;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.companyRank = companyRank;
		this.locationId = locationId;
		this.isAdmin = isAdmin;
	}

	public boolean hasChanged()
	{
		return hasChanged;
	}
	
	public String getCNP()
	{
		return CNP;
	}
	public void setCNP(final String CNP)
	{
		this.CNP = CNP;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(final String firstName)
	{
		if (!firstName.equals(this.firstName))
		{
			this.firstName = firstName;
			hasChanged = true;
		}
	}
	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(final String lastName)
	{
		if (!lastName.equals(this.lastName))
		{
			this.lastName = lastName;
			hasChanged = true;
		}
	}
	public Date getBirthday()
	{
		return birthday;
	}
	public void setBirthday(final Date birthday)
	{
		if (!birthday.equals(this.birthday))
		{
			this.birthday = birthday;
			hasChanged = true;
		}
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		if (!address.equals(this.address))
		{
			this.address = address;
			hasChanged = true;
		}
	}

	public boolean isAdmin()
	{
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin)
	{
		if (this.isAdmin != isAdmin)
		{
			this.isAdmin = isAdmin;
			hasChanged = true;
		}
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		if (!phoneNumber.equals(this.phoneNumber))
		{
			this.phoneNumber = phoneNumber;
			hasChanged = true;
		}
	}

	public String getCompanyRank()
	{
		return companyRank;
	}

	public void setCompanyRank(String companyRank)
	{
		if (!companyRank.equals(this.companyRank))
		{
			this.companyRank = companyRank;
			hasChanged = true;
		}
	}

	public int getLocationId()
	{
		return locationId;
	}

	public void setLocationId(int locationId)
	{
		this.locationId = locationId;
	}

	public String getLocationName()
	{
		return locationName;
	}

	public void setLocationName(String locationName)
	{
		this.locationName = locationName;
	}

	public void setHasChanged(boolean value)
	{
		this.hasChanged = value;
		
	}
}

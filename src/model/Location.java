package model;

import java.io.Serializable;

public class Location implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	private boolean hasChanged;
	
	private int id;
	private String name;
	private String address;
	private int perimeterId;
	private int clients;
	private int admins;
	
	private String perimeterName;
	
	public Location(int _id, String _name, String _address, int _perimeterId)
	{
		id = _id;
		name = _name;
		address = _address;
		perimeterId = _perimeterId;
		hasChanged = false;
		setClients(0);
		setAdmins(0);
	}
	
	public Location(String name, String address, int perimeterId)
	{
		this(-1, name, address, perimeterId);
	}
	
	public boolean hasChanged()
	{
		return hasChanged;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		if (!name.equals(this.name))
		{
			this.name = name;
			hasChanged = true;
		}
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		if (!address.equals(this.address))
		{
			this.address = address;
			hasChanged = true;
		}
	}

	public int getPerimeterId()
	{
		return perimeterId;
	}

	public void setPerimeterId(int perimeterId)
	{
		this.perimeterId = perimeterId;
	}

	public int getAdmins()
	{
		return admins;
	}

	public void setAdmins(int admins)
	{
		this.admins = admins;
	}

	public String getPerimeterName()
	{
		return perimeterName;
	}

	public void setPerimeterName(String perimeterName)
	{
		this.perimeterName = perimeterName;
	}

	public int getClients()
	{
		return clients;
	}

	public void setClients(int clients)
	{
		this.clients = clients;
	}

	public void setHasChanged(boolean value)
	{
		hasChanged = value;	
	}
}

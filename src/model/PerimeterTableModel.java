package model;

import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class PerimeterTableModel extends AbstractTableModel
{
	private ArrayList<Perimeter> rowData = new ArrayList<Perimeter>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public PerimeterTableModel(final ArrayList<Perimeter> perimeters)
	{
		rowData = perimeters;
		columnNames.add("ID");
		columnNames.add("Name");
		columnNames.add("Locality");
		columnNames.add("Locations");
	}

	public Class<?> getColumnClass(int column)
	{
		switch (column)
		{
		case 0:
			return Integer.class;
		case 1:
			return String.class;
		case 2:
			return String.class;
		case 3:
			return Integer.class;
		default:
			return String.class;
		}
	}

	public ArrayList<Perimeter> getChangedPerimeters()
	{
		ArrayList<Perimeter> changed = new ArrayList<Perimeter>(getRowCount());

		for (Perimeter p : rowData)
		{
			if (p.hasChanged())
			{
				changed.add(p);
			}
		}

		return changed;
	}

	public Perimeter getPerimeterAt(int rowIndex)
	{
		return rowData.get(rowIndex);
	}

	public int getColumnCount()
	{
		return columnNames.size();
	}

	public int getRowCount()
	{
		return rowData.size();
	}

	public String getColumnName(int col)
	{
		return columnNames.get(col);
	}

	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
		if (columnIndex > 0 && columnIndex < 3)
			return true;

		return false;
	}

	public Object getValueAt(int rowIndex, int columnIndex)
	{
		Perimeter perim = getPerimeterAt(rowIndex);
		switch (columnIndex)
		{
		case 0:
			return perim.getId();
		case 1:
			return perim.getName();
		case 2:
			return perim.getLocality();
		case 3:
			return perim.getLocations();
		default:
			return null;
		}
	}

	public void setValueAt(Object value, int rowIndex, int columnIndex)
	{
		Perimeter perim = rowData.get(rowIndex);
		switch (columnIndex)
		{
		case 1:
			if (verifyInput(value.toString(), "^[a-zA-Z0-9][a-zA-Z0-9-. ]{3,}"))
				perim.setName(value.toString());
			break;
		case 2:
			if (verifyInput(value.toString(), "^[a-zA-Z0-9][a-zA-Z0-9-. ]{3,}"))
				perim.setLocality(value.toString());
			break;
		}
		fireTableRowsUpdated(rowIndex, rowIndex);
	}

	public ArrayList<Perimeter> getRowData()
	{
		return rowData;
	}

	public void setRowData(ArrayList<Perimeter> rowData)
	{
		this.rowData = rowData;
	}
	
	public boolean verifyInput(String input, String regex)
	{
		final Pattern pattern = Pattern.compile(regex);
		
	    if (!pattern.matcher(input).matches()) 
	    {
	    	JOptionPane.showMessageDialog(null,
				    "Invalid input! Please enter a valid input.",
				    "Error!",
				    JOptionPane.ERROR_MESSAGE);
	        return false;
	    }
	    else
	    {
	    	return true;
	    }
	}
}

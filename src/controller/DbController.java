package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import controller.client.ClientController;
import controller.location.LocationController;
import controller.perimeter.PerimeterController;

public class DbController 
{
	private PerimeterController perimCtrl;
	private LocationController locCtrl;
	private ClientController clientCtrl;
	
	private static final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	private static final String DB_URL = "jdbc:sqlserver://DESKTOP-B0NG6ND;DatabaseName=PrivateSecurityCompanyDb;integratedSecurity=true";
	//private static final String DB_URL = "jdbc:sqlserver://DESKTOP-IL5FVHM;DatabaseName=PrivateSecurityCompanyDb;integratedSecurity=true";
	   
	private Connection dbConnection;
	
	public DbController() 
	{
		try 
		{
			Class.forName(JDBC_DRIVER);
			this.dbConnection = DriverManager.getConnection(DB_URL);
			
			perimCtrl = new PerimeterController(dbConnection);
			locCtrl = new LocationController(this);
			clientCtrl = new ClientController(this);
		} 
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	public Connection getDbConnection()
	{
		return dbConnection;
	}

	public void openConnection()
	{
		try
		{
			this.dbConnection = DriverManager.getConnection(DB_URL);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public PerimeterController getPerimCtrl()
	{
		return perimCtrl;
	}

	public LocationController getLocCtrl()
	{
		return locCtrl;
	}

	public ClientController getClientCtrl()
	{
		return clientCtrl;
	}
}

package controller.perimeter.listener;

import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import controller.DbActionListener;
import controller.DbController;
import controller.location.LocationController;
import controller.perimeter.PerimeterController;
import view.ShowLocationsFrame;

public class ShowLocationsListener extends DbActionListener
{
	private LocationController locCtrl;
	private JPanel mainTablePanel;

	public ShowLocationsListener(final DbController _DbCtrl, final JPanel _mainTablePanel)
	{
		ShowLocationsListener.DbCtrl = _DbCtrl;
		mainTablePanel = _mainTablePanel;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		JFrame mainFrame = (JFrame) SwingUtilities.getRoot(mainTablePanel);

		PerimeterController perimCtrl = DbCtrl.getPerimCtrl();
		int selectedPerimeterId = perimCtrl.getSelectedPerimeterId();

		if (selectedPerimeterId == -1)
		{
			JOptionPane.showMessageDialog(mainFrame, "Please select a perimeter and try again!",
					"No perimeter selected!", JOptionPane.ERROR_MESSAGE);
		} 
		else if (perimCtrl.getPerimeters().get(perimCtrl.getPerimIndexById(selectedPerimeterId)).getLocations() == 0)
		{
			JOptionPane.showMessageDialog(mainFrame, "This perimeter has no locations!",
					"No locations to display", JOptionPane.ERROR_MESSAGE);
		}
		else
		{
			locCtrl = new LocationController(DbCtrl, true, selectedPerimeterId);

			mainFrame.setEnabled(false);

			ShowLocationsFrame showLocFrame = new ShowLocationsFrame(DbCtrl, mainTablePanel, locCtrl);
			showLocFrame.setLocationRelativeTo(mainFrame);
			showLocFrame.setVisible(true);
		}
	}
}

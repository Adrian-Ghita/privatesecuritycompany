package controller.perimeter.listener;

import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import controller.DbActionListener;
import controller.DbController;
import model.Perimeter;

public class AddPerimeterListener extends DbActionListener
{
	private JPanel tablePanel;
	private JTextField txtName;
	private JTextField txtLocality;
	
	public AddPerimeterListener(final DbController _DbCtrl,
							final JPanel _tablePanel,
							final JTextField _txtName,
							final JTextField _txtLocality)
	{
		AddPerimeterListener.DbCtrl = _DbCtrl;
		tablePanel = _tablePanel;
		txtName = _txtName;
		txtLocality = _txtLocality;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		Perimeter newPerimeter = new Perimeter(txtName.getText(), txtLocality.getText());
		
		DbCtrl.getPerimCtrl().addPerimeter(newPerimeter);
		DbCtrl.getPerimCtrl().showPerimeters(tablePanel);
		
		Component component = (Component) e.getSource();
        JFrame frame = (JFrame) SwingUtilities.getRoot(component);
        SwingUtilities.getRoot(tablePanel).setEnabled(true);
        frame.dispose();
	}
}

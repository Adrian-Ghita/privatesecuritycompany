package controller.perimeter;

import java.sql.*;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import common.StyledTable;
import model.Perimeter;
import model.PerimeterTableModel;

public class PerimeterController
{
	private ArrayList<Perimeter> perimeters;
	private int selectedPerimeterId;
	private PerimeterTableModel ptm;
	private StyledTable perimetersTable;
	private Connection dbConnection;

	public PerimeterController(final Connection _dbConnection)
	{
		this.dbConnection = _dbConnection;
		perimeters = new ArrayList<Perimeter>();
		selectedPerimeterId = -1;
		try
		{
			ResultSet perimResult = dbConnection.prepareStatement("SELECT * FROM Perimeter").executeQuery();
			while (perimResult.next())
			{
				Perimeter perim = new Perimeter(perimResult.getInt("Id"), perimResult.getString("Name"),
						perimResult.getString("Locality"));

				perimeters.add(perim);
			}

			retrieveNoOfLocations();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void updatePerimeters(final JFrame mainFrame)
	{
		ArrayList<Perimeter> changed = new ArrayList<Perimeter>();
		changed = ptm.getChangedPerimeters();
		boolean changesMade = false;

		try
		{
			for (Perimeter perim : changed)
			{
				if (perim != null)
				{
					dbConnection.prepareStatement("UPDATE Perimeter SET Name='" + perim.getName() + "', " + "Locality='"
							+ perim.getLocality() + "' " + "WHERE Id='" + perim.getId() + "'").executeUpdate();
					changesMade = true;
					perim.setHasChanged(false);
				}
			}

			if (changesMade)
			{
				JOptionPane.showMessageDialog(mainFrame, "Changes successfully saved!", "Success!",
						JOptionPane.INFORMATION_MESSAGE);
			}
			else
			{
				JOptionPane.showMessageDialog(mainFrame, "No changes were made!", "Information",
						JOptionPane.INFORMATION_MESSAGE);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void addPerimeter(final Perimeter newPerimeter)
	{
		String stmntNameVar = "('" + newPerimeter.getName() + "'";
		String stmntLocalityVar = ", '" + newPerimeter.getLocality() + "')";
		String statementString = "INSERT INTO Perimeter VALUES " + stmntNameVar + stmntLocalityVar;

		try
		{
			dbConnection.prepareStatement(statementString).executeUpdate();

			perimeters.add(newPerimeter);
			if (perimeters != null && !perimeters.isEmpty())
			{
				ResultSet rs = dbConnection.prepareStatement("SELECT TOP 1 Id FROM Perimeter ORDER BY Id DESC")
						.executeQuery();

				if (rs.next())
				{
					perimeters.get(perimeters.size() - 1).setId(rs.getInt("Id"));
				}
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deletePerimeter(final JFrame mainFrame)
	{
		try
		{
			dbConnection.prepareStatement("DELETE FROM Perimeter WHERE Id = " + selectedPerimeterId).executeUpdate();
			perimeters.remove(perimeters.get(getPerimIndexById(selectedPerimeterId)));
		} catch (SQLException e)
		{
			if (e.getSQLState().startsWith("23"))
			{
				JOptionPane.showMessageDialog(mainFrame,
						"One or more locations are linked to this perimeter. Please delete those first and try again!",
						"Error removing perimeter", JOptionPane.ERROR_MESSAGE);
			} else
				e.printStackTrace();
		}
	}

	public void retrieveNoOfLocations()
	{
		try
		{
			ResultSet perimResult = dbConnection
					.prepareStatement("SELECT Perimeter.Id, Count(Location.Id) AS Locations "
							+ "FROM Perimeter JOIN Location ON Perimeter.Id = Location.PerimeterId GROUP BY Perimeter.Id")
					.executeQuery();
			
			for (Perimeter p : perimeters)
			{
				p.setLocations(0);
			}

			while (perimResult.next())
			{
				int index = getPerimIndexById(perimResult.getInt("Id"));
				if (index != -1)
					perimeters.get(index).setLocations(perimResult.getInt("Locations"));
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void showPerimeters(JPanel tablePanel)
	{
		retrieveNoOfLocations();
		ptm = new PerimeterTableModel(perimeters);
		perimetersTable = new StyledTable(ptm, "perimeter");

		ListSelectionModel perimetersTableLSM = perimetersTable.getSelectionModel();
		perimetersTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (!perimetersTableLSM.isSelectionEmpty())
				{
					int selectedPerimeterIndex = perimetersTable
							.convertRowIndexToModel(perimetersTableLSM.getMinSelectionIndex());
					selectedPerimeterId = ptm.getPerimeterAt(selectedPerimeterIndex).getId();
				}
			}
		});

		tablePanel.add(new JScrollPane(perimetersTable));
		tablePanel.validate();
	}

	public ArrayList<Perimeter> getPerimeters()
	{
		return perimeters;
	}

	public int getPerimIndexById(int id)
	{
		for (int i = 0; i < perimeters.size(); i++)
		{
			if (perimeters.get(i).getId() == id)
			{
				return i;
			}
		}
		return -1;
	}

	public StyledTable getPerimetersTable()
	{
		return perimetersTable;
	}

	public int getSelectedPerimeterId()
	{
		return selectedPerimeterId;
	}
}

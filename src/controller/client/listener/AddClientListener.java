package controller.client.listener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.jdatepicker.JDatePicker;

import controller.DbActionListener;
import controller.DbController;

import model.Client;
import model.Location;

@SuppressWarnings("rawtypes")
public class AddClientListener extends DbActionListener
{
	private JPanel tablePanel;
	private JTextField txtCNP;
	private JTextField txtFName;
	private JTextField txtLName;
	private JDatePicker selectedDate;
	private JTextField txtAddress;
	private JTextField txtPhone;
	private JTextField txtRank;
	private JComboBox cmbLoc;

	public AddClientListener(final DbController _DbCtrl, JPanel tablePanel, JTextField txtCNP, JTextField txtFName,
			JTextField txtLName, JDatePicker selectedDate, JTextField txtAddress, JTextField txtPhone,
			JTextField txtRank, JComboBox cmbLoc)
	{
		AddClientListener.DbCtrl = _DbCtrl;
		this.tablePanel = tablePanel;
		this.txtCNP = txtCNP;
		this.txtFName = txtFName;
		this.txtLName = txtLName;
		this.selectedDate = selectedDate;
		this.txtAddress = txtAddress;
		this.txtPhone = txtPhone;
		this.txtRank = txtRank;
		this.cmbLoc = cmbLoc;
	}

	public int getLocIdByName(String name)
	{
		ArrayList<Location> locations = DbCtrl.getLocCtrl().getLocations();
		for (int i = 0; i < locations.size(); i++)
		{
			if (locations.get(i).getName().contentEquals(name))
			{
				return locations.get(i).getId();
			}
		}
		return -1;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		Component component = (Component) e.getSource();
		JFrame frame = (JFrame) SwingUtilities.getRoot(component);

		String selectedLocName = cmbLoc.getSelectedItem().toString();
		Date birthday = (Date) selectedDate.getModel().getValue();

		if (birthday == null)
		{
			JOptionPane.showMessageDialog(frame,
					"Please select a birthday!",
					"Error - submit form", JOptionPane.ERROR_MESSAGE);
		} 
		else
		{
			Client newClient = new Client(txtCNP.getText(), txtFName.getText(), txtLName.getText(), birthday,
					txtAddress.getText(), true, txtPhone.getText(), txtRank.getText(), getLocIdByName(selectedLocName));
			newClient.setLocationName(selectedLocName);

			DbCtrl.getClientCtrl().addClient(newClient);
			DbCtrl.getClientCtrl().showClients(tablePanel);
			SwingUtilities.getRoot(tablePanel).setEnabled(true);
			frame.dispose();
		}
	}
}

package controller.client;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import common.StyledTable;
import controller.DbController;
import model.AdminTableModel;
import model.Client;
import model.ClientTableModel;
import model.Location;

public class ClientController
{
	private ArrayList<Client> clients;
	private String selectedCNP;
	private AbstractTableModel ctm;
	private StyledTable clientsTable;
	private Connection dbConnection;
	private boolean showClients;
	
	public ClientController(final DbController _DbCtrl)
	{
		this(_DbCtrl, false, -1);
	}
	
	public ClientController(final DbController _DbCtrl, boolean showClients, int locationId)
	{
		dbConnection = _DbCtrl.getDbConnection();
		clients = new ArrayList<Client>();
		selectedCNP = "";
		this.showClients = showClients;
		
		if(!showClients)
			retrieveAllClients(_DbCtrl);
		else
			retrieveLocationAdmins(_DbCtrl, locationId);
		
	}
	
	public void retrieveAllClients(final DbController _DbCtrl)
	{
		try
		{
			ResultSet clientsResult = dbConnection.prepareStatement("SELECT * FROM Admin").executeQuery();
			while (clientsResult.next())
			{
				int locationId = clientsResult.getInt("LocationId");
				
				Client client = new Client(clientsResult.getString("CNP"), clientsResult.getString("FirstName"),
							clientsResult.getString("LastName"), clientsResult.getDate("Birthday"),
							clientsResult.getString("HomeAddress"), clientsResult.getBoolean("IsAdmin"),
							clientsResult.getString("PhoneNumber"), clientsResult.getString("CompanyRank"), locationId);

				client.setLocationName(getLocationName(_DbCtrl, locationId));
				clients.add(client);
			}
			
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void retrieveLocationAdmins(final DbController _DbCtrl, final int locationId)
	{
		try
		{
			ResultSet clientsResult = dbConnection.prepareStatement("SELECT * FROM Admin"
					+ " WHERE LocationId = " + locationId + " AND IsAdmin = 'true'").executeQuery();
			while (clientsResult.next())
			{		
				Client client = new Client(clientsResult.getString("CNP"), clientsResult.getString("FirstName"),
							clientsResult.getString("LastName"), clientsResult.getDate("Birthday"),
							clientsResult.getString("HomeAddress"), true, clientsResult.getString("PhoneNumber"),
							clientsResult.getString("CompanyRank"), locationId);

				client.setLocationName(getLocationName(_DbCtrl, locationId));
				clients.add(client);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public String getLocationName(DbController DbCtrl, int id)
	{
		ArrayList<Location> locations = DbCtrl.getLocCtrl().getLocations();

		for (int i = 0; i < locations.size(); i++)
		{
			if (locations.get(i).getId() == id)
			{
				return locations.get(i).getName();
			}
		}

		return "";
	}
	
	public void addClient(final Client newClient)
	{
		String stmntCNP = "('" + newClient.getCNP() + "'";
		String stmntFirstName = ", '" + newClient.getFirstName() + "'";
		String stmntLastName = ", '" + newClient.getLastName() + "'";
		String stmntBirthday = ", '" + newClient.getBirthday() + "'";
		String stmntAddress = ", '" + newClient.getAddress() + "'";
		String stmntAdmin = ", 'true'";
		String stmntPhone = ", '" + newClient.getPhoneNumber() + "'";
		String stmntRank = ", '" + newClient.getCompanyRank() + "'";
		String stmntLocationId = ", " + newClient.getLocationId() + ")";

		String statementString = "INSERT INTO Admin VALUES " + stmntCNP + stmntFirstName + stmntLastName
							+ stmntBirthday + stmntAddress + stmntAdmin + stmntPhone + stmntRank + stmntLocationId;

		try
		{
			dbConnection.prepareStatement(statementString).executeUpdate();
			clients.add(newClient);

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteClient(final JFrame mainFrame)
	{
		try
		{
			dbConnection.prepareStatement("DELETE FROM Admin WHERE CNP = '" + selectedCNP + "'").executeUpdate();
			clients.remove(clients.get(findClientByCNP(selectedCNP)));
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public int findClientByCNP(String cnp)
	{
		for (int i = 0; i < clients.size(); i++)
		{
			if (clients.get(i).getCNP().contentEquals(cnp))
			{
				return i;
			}
		}
		return -1;
	}
	
	public void updateClients(final JFrame mainFrame)
	{
		ArrayList<Client> changed = new ArrayList<Client>();
		changed = ((ClientTableModel) ctm).getChangedClients();
		boolean changesMade = false;

		try
		{
			for (Client client : changed)
			{
				if (client != null)
				{
					dbConnection.prepareStatement("UPDATE Admin SET FirstName='" + client.getFirstName() +
							"', LastName='" + client.getLastName() + 
							"', Birthday='" + client.getBirthday() + 
							"', HomeAddress='" + client.getAddress() + 
							"', isAdmin='" + client.isAdmin() + 
							"', PhoneNumber='" + client.getPhoneNumber() + 
							"', CompanyRank='" + client.getCompanyRank() + 
							"' WHERE CNP='" + client.getCNP() + "'").executeUpdate();
					
					changesMade = true;
					client.setHasChanged(false);
				}
			}

			if (changesMade)
			{
				JOptionPane.showMessageDialog(mainFrame, "Changes successfully saved!", "Success!",
						JOptionPane.INFORMATION_MESSAGE);
			}
			else
			{
				JOptionPane.showMessageDialog(mainFrame, "No changes were made!", "Information",
						JOptionPane.INFORMATION_MESSAGE);
			}
			
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void showClients(JPanel tablePanel)
	{
		ctm = showClients ? new AdminTableModel(clients) : new ClientTableModel(clients);
		clientsTable = showClients ? new StyledTable(ctm, "admin") : new StyledTable(ctm, "client");

		ListSelectionModel clientsTableLSM = clientsTable.getSelectionModel();
		clientsTableLSM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		clientsTableLSM.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (!clientsTableLSM.isSelectionEmpty())
				{
					selectedCNP = (String) ctm.getValueAt(clientsTable.convertRowIndexToModel(clientsTableLSM.getMinSelectionIndex()), 0);
				}
			}
		});

		tablePanel.add(new JScrollPane(clientsTable));
		tablePanel.validate();
	}

	public StyledTable getClientsTable()
	{
		return clientsTable;
	}

	public ArrayList<Client> getClients()
	{
		return clients;
	}
}

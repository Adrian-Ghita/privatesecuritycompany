package controller.client;

import java.awt.Color;
import java.util.BitSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;

public class AddClientRegexInputVerifier extends InputVerifier
{
	private String expression;
	private JButton btnOk;
	private BitSet txtFldsVerified;

	public AddClientRegexInputVerifier(String expression, JButton _btnOk, BitSet _txtFldsVerified)
	{
		this.expression = expression;
		btnOk = _btnOk;
		txtFldsVerified = _txtFldsVerified;
	}

	public String getExpression()
	{
		return expression;
	}

	@Override
	public boolean verify(JComponent input)
	{
		boolean verified = false;
		if (input instanceof JTextComponent)
		{
			JTextComponent field = (JTextComponent) input;
			String regExpression = field.getText();

			Pattern pattern = Pattern.compile(expression);
			Matcher matcher = pattern.matcher(regExpression);

			if (matcher.matches())
			{
				verified = true;
				field.setBackground(Color.WHITE);

				switch(field.getName())
				{
				case "cnp":
					txtFldsVerified.set(0);
					break;
				case "fname":
					txtFldsVerified.set(1);
					break;
				case "lname":
					txtFldsVerified.set(2);
					break;
				case "address":
					txtFldsVerified.set(3);
					break;
				case "phone":
					txtFldsVerified.set(4);
					break;
				case "rank":
					txtFldsVerified.set(5);
					break;
				}
			} 
			else
			{
				field.setBackground(new Color(255, 200, 200));
				
				switch(field.getName())
				{
				case "cnp":
					txtFldsVerified.set(0, false);
					break;
				case "fname":
					txtFldsVerified.set(1, false);
					break;
				case "lname":
					txtFldsVerified.set(2, false);
					break;
				case "address":
					txtFldsVerified.set(3, false);
					break;
				case "phone":
					txtFldsVerified.set(4, false);
					break;
				case "rank":
					txtFldsVerified.set(5, false);
					break;
				}
			}
		}
		btnOk.setEnabled(txtFldsVerified.cardinality() == 6 ? true : false);

		return verified;
	}
}

package controller.location;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import common.StyledTable;
import controller.DbController;
import model.Location;
import model.LocationTableModel;
import model.Perimeter;

public class LocationController
{
	private ArrayList<Location> locations;
	private LocationTableModel ltm;
	private StyledTable locationsTable;
	private int selectedLocationId;
	private Connection dbConnection;

	public LocationController(final DbController _DbCtrl)
	{
		this(_DbCtrl, false, -1);
	}

	public LocationController(final DbController _DbCtrl, boolean showLocations, int perimeterId)
	{
		dbConnection = _DbCtrl.getDbConnection();
		locations = new ArrayList<Location>();
		selectedLocationId = -1;

		if (!showLocations)
			retrieveAllLocations(_DbCtrl);
		else
			retrievePerimeterLocations(_DbCtrl, perimeterId);
	}

	public void retrieveAllLocations(final DbController _DbCtrl)
	{
		try
		{
			ResultSet locResult = dbConnection.prepareStatement("SELECT * FROM Location").executeQuery();
			while (locResult.next())
			{
				int perimId = locResult.getInt("PerimeterId");
				Location loc = new Location(locResult.getInt("Id"), locResult.getString("Name"),
						locResult.getString("LocationAddress"), perimId);

				loc.setPerimeterName(getPerimeterName(_DbCtrl, perimId));
				locations.add(loc);
			}

			retrieveNoOfClients();
			retrieveNoOfAdmins();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void retrievePerimeterLocations(final DbController _DbCtrl, final int perimeterId)
	{
		try
		{
			ResultSet locResult = dbConnection
					.prepareStatement("SELECT * FROM Location " + "WHERE PerimeterId = " + perimeterId).executeQuery();
			while (locResult.next())
			{
				Location loc = new Location(locResult.getInt("Id"), locResult.getString("Name"),
						locResult.getString("LocationAddress"), perimeterId);

				loc.setPerimeterName(getPerimeterName(_DbCtrl, perimeterId));
				locations.add(loc);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public String getPerimeterName(DbController DbCtrl, int id)
	{
		ArrayList<Perimeter> perimeters = DbCtrl.getPerimCtrl().getPerimeters();

		for (int i = 0; i < perimeters.size(); i++)
		{
			if (perimeters.get(i).getId() == id)
			{
				return perimeters.get(i).getName();
			}
		}

		return "";
	}

	public void addLocation(final Location newLocation)
	{
		String stmntName = "('" + newLocation.getName() + "'";
		String stmntAddress = ", '" + newLocation.getAddress() + "'";
		String stmntPerimId = ", " + newLocation.getPerimeterId() + ")";

		String statementString = "INSERT INTO Location VALUES " + stmntName + stmntAddress + stmntPerimId;

		try
		{
			dbConnection.prepareStatement(statementString).executeUpdate();
			locations.add(newLocation);

			if (locations != null && !locations.isEmpty())
			{
				ResultSet rs = dbConnection.prepareStatement("SELECT TOP 1 Id FROM Location ORDER BY Id DESC")
						.executeQuery();

				if (rs.next())
				{
					locations.get(locations.size() - 1).setId(rs.getInt("Id"));
				}
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void updateLocations(final JFrame mainFrame)
	{
		ArrayList<Location> changed = new ArrayList<Location>();
		changed = ltm.getChangedLocations();
		boolean changesMade = false;

		try
		{
			for (Location loc : changed)
			{
				if (loc != null)
				{
					dbConnection.prepareStatement("UPDATE Location SET Name='" + loc.getName() + "', "
							+ "LocationAddress='" + loc.getAddress() + "' " + " WHERE Id='" + loc.getId() + "'")
							.executeUpdate();

					changesMade = true;
					loc.setHasChanged(false);
				}
			}

			if (changesMade)
			{
				JOptionPane.showMessageDialog(mainFrame, "Changes successfully saved!", "Success!",
						JOptionPane.INFORMATION_MESSAGE);
			} else
			{
				JOptionPane.showMessageDialog(mainFrame, "No changes were made!", "Information",
						JOptionPane.INFORMATION_MESSAGE);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteLocation(final JFrame mainFrame)
	{
		try
		{
			dbConnection.prepareStatement("DELETE FROM Location WHERE Id = " + selectedLocationId).executeUpdate();
			locations.remove(locations.get(getLocationIndexById(selectedLocationId)));

		} catch (SQLException e)
		{
			if (e.getSQLState().startsWith("23"))
			{
				JOptionPane.showMessageDialog(mainFrame,
						"One or more clients are linked to this location. Please delete those first and try again!",
						"Error removing location", JOptionPane.ERROR_MESSAGE);
			} else
				e.printStackTrace();
		}
	}

	public void retrieveNoOfClients()
	{
		try
		{
			ResultSet locResult = dbConnection
					.prepareStatement("SELECT Location.Id, Count(Admin.CNP) AS Clients"
							+ " FROM Location JOIN Admin ON Location.Id = Admin.LocationId GROUP BY Location.Id")
					.executeQuery();

			for (Location l : locations)
			{
				l.setAdmins(0);
			}

			while (locResult.next())
			{
				int index = getLocationIndexById(locResult.getInt("Id"));
				if (index != -1)
					locations.get(index).setClients(locResult.getInt("Clients"));
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void retrieveNoOfAdmins()
	{
		try
		{
			ResultSet locResult = dbConnection.prepareStatement("SELECT Location.Id, Count(Admin.CNP) AS Admins"
					+ " FROM Location JOIN Admin ON Location.Id = Admin.LocationId AND"
					+ " Admin.IsAdmin = 'true' GROUP BY Location.Id").executeQuery();

			for (Location l : locations)
			{
				l.setAdmins(0);
			}

			while (locResult.next())
			{
				int index = getLocationIndexById(locResult.getInt("Id"));
				if (index != -1)
					locations.get(index).setAdmins(locResult.getInt("Admins"));
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void showLocations(JPanel tablePanel)
	{
		retrieveNoOfClients();
		retrieveNoOfAdmins();
		ltm = new LocationTableModel(locations);

		locationsTable = new StyledTable(ltm, "location");

		ListSelectionModel locationsTableLSM = locationsTable.getSelectionModel();
		locationsTableLSM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		locationsTableLSM.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (!locationsTableLSM.isSelectionEmpty())
				{
					int selectedLocationIndex = locationsTable
							.convertRowIndexToModel(locationsTableLSM.getMinSelectionIndex());
					selectedLocationId = ltm.getLocationAt(selectedLocationIndex).getId();
				}
			}
		});

		tablePanel.add(new JScrollPane(locationsTable));
		tablePanel.validate();
	}

	public int getLocationIndexById(int id)
	{
		for (int i = 0; i < locations.size(); i++)
		{
			if (locations.get(i).getId() == id)
			{
				return i;
			}
		}
		return -1;
	}

	public StyledTable getLocationsTable()
	{
		return locationsTable;
	}

	public ArrayList<Location> getLocations()
	{
		return locations;
	}

	public int getSelectedLocationId()
	{
		return selectedLocationId;
	}
}

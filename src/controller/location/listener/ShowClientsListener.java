package controller.location.listener;

import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import controller.DbActionListener;
import controller.DbController;
import controller.client.ClientController;
import controller.location.LocationController;
import controller.perimeter.listener.ShowLocationsListener;
import view.ShowClientsFrame;

public class ShowClientsListener extends DbActionListener
{
	private ClientController clientCtrl;
	private JPanel mainTablePanel;

	public ShowClientsListener(final DbController _DbCtrl, final JPanel _mainTablePanel)
	{
		ShowLocationsListener.DbCtrl = _DbCtrl;
		mainTablePanel = _mainTablePanel;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		JFrame mainFrame = (JFrame) SwingUtilities.getRoot(mainTablePanel);

		LocationController locCtrl = DbCtrl.getLocCtrl();
		int selectedLocationId = locCtrl.getSelectedLocationId();

		if (selectedLocationId == -1)
		{
			JOptionPane.showMessageDialog(mainFrame, "Please select a location and try again!",
					"No location selected!", JOptionPane.ERROR_MESSAGE);
		} 
		else if (locCtrl.getLocations().get(locCtrl.getLocationIndexById(selectedLocationId)).getAdmins() == 0)
		{
			JOptionPane.showMessageDialog(mainFrame, "This location has no admins!",
					"No admins to display", JOptionPane.ERROR_MESSAGE);
		}
		else
		{
			clientCtrl = new ClientController(DbCtrl, true, selectedLocationId);

			mainFrame.setEnabled(false);

			ShowClientsFrame showClientsFrame = new ShowClientsFrame(DbCtrl, mainTablePanel, clientCtrl);
			showClientsFrame.setLocationRelativeTo(mainFrame);
			showClientsFrame.setVisible(true);
		}
	}
}

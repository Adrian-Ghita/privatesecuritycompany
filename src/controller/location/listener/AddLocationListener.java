package controller.location.listener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import controller.DbActionListener;
import controller.DbController;
import model.Location;
import model.Perimeter;

@SuppressWarnings("rawtypes")
public class AddLocationListener extends DbActionListener
{
	private JPanel tablePanel;
	private JTextField txtName;
	private JTextField txtAddress;
	private JComboBox cmbPerim;
	
	public AddLocationListener(DbController _DbCtrl, JPanel tablePanel, JTextField txtName, 
							JTextField txtAddress, JComboBox cmbPerim)
	{
		AddLocationListener.DbCtrl = _DbCtrl;
		this.tablePanel = tablePanel;
		this.txtName = txtName;
		this.txtAddress = txtAddress;
		this.cmbPerim = cmbPerim;
	}

	public int getPerimIdByName(String name)
	{
		ArrayList<Perimeter> perimeters = DbCtrl.getPerimCtrl().getPerimeters();
		for (int i = 0; i < perimeters.size(); i++)
		{
			if (perimeters.get(i).getName().contentEquals(name))
			{
				return perimeters.get(i).getId();
			}
		}
		return -1;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String perimName = cmbPerim.getSelectedItem().toString();
		Location newLocation = new Location(txtName.getText(), txtAddress.getText(), getPerimIdByName(perimName));
		newLocation.setPerimeterName(perimName);
		
		DbCtrl.getLocCtrl().addLocation(newLocation);
		DbCtrl.getLocCtrl().showLocations(tablePanel);
		
		Component component = (Component) e.getSource();
        JFrame frame = (JFrame) SwingUtilities.getRoot(component);
        SwingUtilities.getRoot(tablePanel).setEnabled(true);
        frame.dispose();
	}
}

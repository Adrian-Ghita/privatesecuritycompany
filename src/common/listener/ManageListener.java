package common.listener;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import controller.DbActionListener;
import controller.DbController;

@SuppressWarnings("unused")
public class ManageListener extends DbActionListener implements ItemListener
{
	private JPanel buttonsPanel;
	private JPanel tablePanel;
	private JPanel filterPanel;

	public ManageListener(final DbController _DbCtrl, 
			final JPanel _filterPanel, 
			final JPanel _buttonsPanel,
			final JPanel _tablePanel)
	{
		ManageListener.DbCtrl = _DbCtrl;
		filterPanel = _filterPanel;
		buttonsPanel = _buttonsPanel;
		tablePanel = _tablePanel;
		
		JPanel pnl = (JPanel) filterPanel.getComponents()[0];
		pnl.getComponents()[0].requestFocusInWindow();
		
		DbCtrl.getPerimCtrl().showPerimeters(tablePanel);
	}

	@Override
	public void itemStateChanged(ItemEvent event)
	{
		if (event.getStateChange() == ItemEvent.SELECTED)
		{
			JPanel pnl;
			String selectedItem = event.getItem().toString();
			tablePanel.removeAll();
			
			switch (selectedItem)
			{
			case "Perimeters":
				((CardLayout) buttonsPanel.getLayout()).show(buttonsPanel, "perimeters");
				((CardLayout) filterPanel.getLayout()).show(filterPanel, "filterHosp");
				
				pnl = (JPanel) filterPanel.getComponents()[0];
				pnl.getComponents()[0].requestFocusInWindow();
				
				DbCtrl.getPerimCtrl().showPerimeters(tablePanel);
				break;
			case "Locations":
				((CardLayout) buttonsPanel.getLayout()).show(buttonsPanel, "locations");
				((CardLayout) filterPanel.getLayout()).show(filterPanel, "filterLoc");
				
				pnl = (JPanel) filterPanel.getComponents()[1];
				pnl.getComponents()[0].requestFocusInWindow();
				
				DbCtrl.getLocCtrl().showLocations(tablePanel);
				break;
			case "Clients":
				((CardLayout) buttonsPanel.getLayout()).show(buttonsPanel, "clients");
				((CardLayout) filterPanel.getLayout()).show(filterPanel, "filterClient");
				
				pnl = (JPanel) filterPanel.getComponents()[2];
				pnl.getComponents()[0].requestFocusInWindow();
				
				DbCtrl.getClientCtrl().showClients(tablePanel);
				break;
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) { }
}

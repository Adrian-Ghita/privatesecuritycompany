package common.listener;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import controller.DbActionListener;
import controller.DbController;
import model.Location;
import model.Perimeter;
import view.AddClientFrame;
import view.AddLocationFrame;
import view.AddPerimeterFrame;

public class AddListener extends DbActionListener
{
	private JFrame frmAdd;
	private JPanel tablePanel;
	
	public AddListener(final DbController DbCtrl, 
			final JFrame _frmAdd, 
			final JPanel _tablePanel)
	{
		AddListener.DbCtrl = DbCtrl;
		frmAdd = _frmAdd;
		tablePanel = _tablePanel;
	}	
	
	public void actionPerformed(ActionEvent e)
	{
		JButton btnAdd = (JButton) e.getSource();
		JFrame mainFrame = (JFrame) SwingUtilities.getRoot(btnAdd);
		mainFrame.setEnabled(false);
		
		tablePanel.removeAll();
		
		switch(btnAdd.getName())
		{
		case "addPerimeter":
			frmAdd = new AddPerimeterFrame(AddListener.DbCtrl, tablePanel);				
			frmAdd.setLocationRelativeTo(mainFrame);
			frmAdd.setVisible(true);
			break;
		case "addLocation":
			frmAdd = new AddLocationFrame(AddListener.DbCtrl, tablePanel, getPerimetersNames());
			frmAdd.setLocationRelativeTo(mainFrame);
			frmAdd.setVisible(true);
			break;
		case "addClient":
			frmAdd = new AddClientFrame(AddListener.DbCtrl, tablePanel, getLocationsNames());
			frmAdd.setLocationRelativeTo(mainFrame);
			frmAdd.setVisible(true);
			break;
		}
	}
	
	public String[] getPerimetersNames()
	{
		ArrayList<Perimeter> perimeters = DbCtrl.getPerimCtrl().getPerimeters();
		String[] names = new String[perimeters.size()];
		
		for(int i = 0; i < perimeters.size(); i++)
		{
			names[i] = perimeters.get(i).getName();
		}
		
		return names;
	}
	
	public String[] getLocationsNames()
	{
		ArrayList<Location> locations = DbCtrl.getLocCtrl().getLocations();
		String[] names = new String[locations.size()];
		
		for(int i = 0; i < locations.size(); i++)
		{
			names[i] = locations.get(i).getName();
		}
		
		return names;
	}
}

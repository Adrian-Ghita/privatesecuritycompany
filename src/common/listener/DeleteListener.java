package common.listener;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import controller.DbActionListener;
import controller.DbController;

public class DeleteListener extends DbActionListener
{
	private JPanel tablePanel;
	
	public DeleteListener(final DbController DbCtrl,
						  final JPanel _tablePanel)
	{
		DeleteListener.DbCtrl = DbCtrl;
		tablePanel = _tablePanel;
	}
	
	public void actionPerformed(ActionEvent e)
	{
		JButton btnDel = (JButton) e.getSource();
        JFrame mainFrame = (JFrame) SwingUtilities.getRoot(btnDel);
        tablePanel.removeAll();
		
		switch(btnDel.getName())
		{
		case "delPerimeter":
			DbCtrl.getPerimCtrl().deletePerimeter(mainFrame);
			DbCtrl.getPerimCtrl().showPerimeters(tablePanel);
			break;
		case "delLocation":
			DbCtrl.getLocCtrl().deleteLocation(mainFrame);
			DbCtrl.getLocCtrl().showLocations(tablePanel);
			break;
		case "delClient":
			DbCtrl.getClientCtrl().deleteClient(mainFrame);
			DbCtrl.getClientCtrl().showClients(tablePanel);
			break;
		}
	}
}

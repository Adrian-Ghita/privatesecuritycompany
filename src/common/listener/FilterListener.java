package common.listener;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;
import controller.DbActionListener;
import controller.DbController;
import controller.client.ClientController;
import controller.location.LocationController;

public class FilterListener extends DbActionListener implements KeyListener
{	
	private LocationController locCtrl;
	private ClientController clientCtrl;
	
	public FilterListener(final DbController _DbCtrl)
	{
		FilterListener.DbCtrl = _DbCtrl;
	}
	
	public FilterListener(final LocationController locCtrl)
	{
		this.locCtrl = locCtrl;
	}
	
	public FilterListener(final ClientController clientCtrl)
	{
		this.clientCtrl = clientCtrl;
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		JTextField txtSearch = (JTextField) e.getSource();
		String query = txtSearch.getText();
		
		switch(txtSearch.getName())
		{
		case "fperim":
			DbCtrl.getPerimCtrl().getPerimetersTable().filterResults(query);
			break;
		case "floc":
			DbCtrl.getLocCtrl().getLocationsTable().filterResults(query);
			break;
		case "fclient":
			DbCtrl.getClientCtrl().getClientsTable().filterResults(query);
			break;
		case "fshowloc":
			if (locCtrl != null)
				locCtrl.getLocationsTable().filterResults(query);
		case "fshowclients":
			if (clientCtrl != null)
				clientCtrl.getClientsTable().filterResults(query);
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) { }

	@Override
	public void keyTyped(KeyEvent e) { }

	@Override
	public void actionPerformed(ActionEvent e) { }
}

package common.listener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import controller.DbActionListener;
import controller.DbController;

@SuppressWarnings("rawtypes")
public class ThemeListener extends DbActionListener implements ItemListener
{
	private JComboBox manageCBox;
	private JPanel tablePanel;
	
	public ThemeListener(final DbController dbCtrl, final JFrame frame, final JComboBox _manageCBox, final JPanel _tablePanel)
	{
		ThemeListener.DbCtrl = dbCtrl;
		manageCBox = _manageCBox;
		tablePanel = _tablePanel;
		setLookAndFeel("Nimbus");
		SwingUtilities.updateComponentTreeUI(frame);
	}

	@Override
	public void itemStateChanged(ItemEvent event)
	{
		if (event.getStateChange() == ItemEvent.SELECTED)
		{
			String selectedItem = event.getItem().toString();
			setLookAndFeel(selectedItem);

			Component component = (Component) event.getSource();
			JFrame frame = (JFrame) SwingUtilities.getRoot(component);
			SwingUtilities.updateComponentTreeUI(frame);
			
			tablePanel.removeAll();
			switch(manageCBox.getSelectedItem().toString())
			{
			case "Perimeters":
				DbCtrl.getPerimCtrl().showPerimeters(tablePanel);
				break;
			case "Locations":
				DbCtrl.getLocCtrl().showLocations(tablePanel);
				break;
			case "Clients":
				DbCtrl.getClientCtrl().showClients(tablePanel);
			}
		}
	}

	private static void setLookAndFeel(final String LOOKANDFEEL)
	{
		String lookAndFeel = null;

		if (LOOKANDFEEL != null)
		{
			if (LOOKANDFEEL.equals("Metal"))
			{
				lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
			} 
			else if (LOOKANDFEEL.equals("System"))
			{
				lookAndFeel = UIManager.getSystemLookAndFeelClassName();
			} 
			else if (LOOKANDFEEL.equals("Nimbus"))
			{
				lookAndFeel = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
			} 
			else
			{
				System.err.println("Unexpected value of LOOKANDFEEL specified: " + LOOKANDFEEL);
				lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
			}

			try
			{
				UIManager.setLookAndFeel(lookAndFeel);
				
			} catch (ClassNotFoundException e)
			{
				System.err.println("Couldn't find class for specified look and feel:" + lookAndFeel);
				System.err.println("Did you include the L&F library in the class path?");
				System.err.println("Using the default look and feel.");
			} catch (UnsupportedLookAndFeelException e)
			{
				System.err.println("Can't use the specified look and feel (" + lookAndFeel + ") on this platform.");
				System.err.println("Using the default look and feel.");
			} catch (Exception e)
			{
				System.err.println("Couldn't get specified look and feel (" + lookAndFeel + "), for some reason.");
				System.err.println("Using the default look and feel.");
				e.printStackTrace();
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
	}
}

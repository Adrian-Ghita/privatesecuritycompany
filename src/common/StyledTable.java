package common;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

@SuppressWarnings("serial")
public class StyledTable extends JTable
{
	public StyledTable(AbstractTableModel model, String tableType)
	{
		super(model);
		
		this.setRowSorter(new TableRowSorter<AbstractTableModel>(model));
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		
		centerColumnContents(centerRenderer, tableType);
		
		setColumnsWidth(tableType);
		
		this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
	
	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int Index_row, int Index_col)
	{
		this.setBorder(BorderFactory.createLineBorder(new Color(100, 200, 200), 1));
		this.setRowHeight(22);
		
		JTableHeader tableHeader = this.getTableHeader();
		tableHeader.setBackground(new Color(100, 200, 200));
		tableHeader.setFont(new Font("Tahoma", Font.BOLD, 12));
		tableHeader.setBorder(BorderFactory.createLineBorder(new Color(100, 200, 200), 1));
		
		Component comp = super.prepareRenderer(renderer, Index_row, Index_col);

		if (Index_row % 2 == 0 && !isCellSelected(Index_row, Index_col))
		{
			comp.setBackground(new Color(220, 220, 220));
		} else
		{
			comp.setBackground(Color.white);
		}
		
		if (isCellSelected(Index_row, Index_col))
			comp.setBackground(new Color(255, 193, 102));
		return comp;
	}
	
	@SuppressWarnings("unchecked")
	public void filterResults(final String query)
	{
		TableRowSorter<AbstractTableModel> sorter = (TableRowSorter<AbstractTableModel>) this.getRowSorter();
	    sorter.setRowFilter(RowFilter.regexFilter("(?i)" + query));
	}
	
	public void setColumnsWidth(String tableType)
	{
		switch (tableType)
		{		
		case "perimeter":
			this.getColumnModel().getColumn(1).setPreferredWidth(300);
			this.getColumnModel().getColumn(2).setPreferredWidth(300);
			break;
		case "location":
			this.getColumnModel().getColumn(1).setPreferredWidth(250);
			this.getColumnModel().getColumn(2).setPreferredWidth(200);
			this.getColumnModel().getColumn(4).setPreferredWidth(50);
			this.getColumnModel().getColumn(5).setPreferredWidth(50);
			break;
		case "client":
			this.getColumnModel().getColumn(1).setPreferredWidth(50);
			this.getColumnModel().getColumn(2).setPreferredWidth(40);
			this.getColumnModel().getColumn(3).setPreferredWidth(50);
			this.getColumnModel().getColumn(4).setPreferredWidth(145);
			this.getColumnModel().getColumn(5).setPreferredWidth(15);
			break;
		case "admin":
			this.getColumnModel().getColumn(0).setPreferredWidth(90);
			this.getColumnModel().getColumn(4).setPreferredWidth(165);
			this.getColumnModel().getColumn(6).setPreferredWidth(100);
		}
	}
	
	public void centerColumnContents(DefaultTableCellRenderer centerRenderer, String tableType)
	{
		switch (tableType)
		{
		case "perimeter":
			this.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
			this.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
			break;
		case "location":
			this.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
			this.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
			this.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
			break;
		case "client":
			this.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
			this.getColumnModel().getColumn(6).setCellRenderer(centerRenderer);
			break;
		case "admin":
			this.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
			this.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
		}
	}
}

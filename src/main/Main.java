package main;

import java.awt.EventQueue;

import view.SecurityCompanyGUI;

public class Main
{
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				new SecurityCompanyGUI();
			}
		});
	}
}

package view;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import common.listener.ManageListener;
import common.listener.ThemeListener;
import common.listener.AddListener;
import common.listener.DeleteListener;
import common.listener.FilterListener;
import controller.DbController;
import controller.location.listener.ShowClientsListener;
import controller.perimeter.listener.ShowLocationsListener;

import javax.swing.JComboBox;
import java.awt.Dimension;
import javax.swing.JTextField;
import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.awt.event.ActionEvent;

public class SecurityCompanyGUI 
{
	public JFrame frmBeSafeSecurity;
	private AddPerimeterFrame addPerimFrame;
	private AddLocationFrame addLocFrame;
	private AddClientFrame addClientFrame;
	
	private static DbController DbCtrl;
	private static Properties userSettings = new Properties();
	
	private static final String[] manageOptions = {"Perimeters", "Locations", "Clients"};
	private static final String[] themesNames = {"Nimbus", "Metal", "System"};
	private JTextField txtFilterPerim;
	
	private JPanel buttonsPanel;
	private JPanel contentPanel;
	private JPanel tablePanel;
	
	private JTextField txtFilterLoc;
	private JTextField txtFilterClients;
	
	public SecurityCompanyGUI() 
	{
		initUserSettings();
		initialize();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() 
	{
		frmBeSafeSecurity = new JFrame();
		frmBeSafeSecurity.setBounds(100, 100, 900, 550);
		//frmBeSafeSecurity.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frmBeSafeSecurity.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBeSafeSecurity.getContentPane().setLayout(new BorderLayout(0, 0));
		frmBeSafeSecurity.setTitle("BE SAFE Security Services Management Application");
		
		SecurityCompanyGUI.DbCtrl = new DbController();
		
		JPanel panelChoose = new JPanel();
		frmBeSafeSecurity.getContentPane().add(panelChoose, BorderLayout.WEST);
		GridBagLayout gbl_panelChoose = new GridBagLayout();
		gbl_panelChoose.columnWidths = new int[]{0, 20, 0};
		gbl_panelChoose.rowHeights = new int[]{1, 0, 0, 0};
		gbl_panelChoose.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panelChoose.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0};
		panelChoose.setLayout(gbl_panelChoose);
		
		JLabel lblChoose = new JLabel("Choose what to manage...");
		lblChoose.setFont(new Font("Tahoma", Font.BOLD, 13));
		GridBagConstraints gbc_lblChoose = new GridBagConstraints();
		gbc_lblChoose.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblChoose.insets = new Insets(5, 5, 0, 5);
		gbc_lblChoose.gridx = 0;
		gbc_lblChoose.gridy = 1;
		panelChoose.add(lblChoose, gbc_lblChoose);
		
		JSeparator leftPanelVertSep = new JSeparator();
		leftPanelVertSep.setForeground(new Color(0, 128, 128));
		leftPanelVertSep.setOrientation(SwingConstants.VERTICAL);
		GridBagConstraints gbc_leftPanelVertSep = new GridBagConstraints();
		gbc_leftPanelVertSep.fill = GridBagConstraints.VERTICAL;
		gbc_leftPanelVertSep.gridheight = 4;
		gbc_leftPanelVertSep.insets = new Insets(0, 5, 0, 0);
		gbc_leftPanelVertSep.gridx = 1;
		gbc_leftPanelVertSep.gridy = 0;
		panelChoose.add(leftPanelVertSep, gbc_leftPanelVertSep);
		
		contentPanel = new JPanel();
		frmBeSafeSecurity.getContentPane().add(contentPanel, BorderLayout.SOUTH);
		contentPanel.setPreferredSize(new Dimension(100, 400));
		GridBagLayout gbl_panelContent = new GridBagLayout();
		gbl_panelContent.columnWidths = new int[]{1, 358, 330, 0};
		gbl_panelContent.rowHeights = new int[]{0, 1, 250, 0, 0};
		gbl_panelContent.columnWeights = new double[]{1.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panelContent.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_panelContent);
		
		JLabel lblFilter = new JLabel("Filter results:");
		lblFilter.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFilter.setToolTipText("Filter results by typing anything in the search box");
		lblFilter.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_lblFilter = new GridBagConstraints();
		gbc_lblFilter.insets = new Insets(5, 15, 5, 5);
		gbc_lblFilter.gridx = 0;
		gbc_lblFilter.gridy = 1;
		contentPanel.add(lblFilter, gbc_lblFilter);
		
		JPanel panelFilter = new JPanel();
		GridBagConstraints gbc_panelFilter = new GridBagConstraints();
		gbc_panelFilter.gridwidth = 2;
		gbc_panelFilter.insets = new Insets(0, 0, 5, 5);
		gbc_panelFilter.fill = GridBagConstraints.BOTH;
		gbc_panelFilter.gridx = 1;
		gbc_panelFilter.gridy = 1;
		contentPanel.add(panelFilter, gbc_panelFilter);
		panelFilter.setLayout(new CardLayout(0, 0));
		
		JPanel pnlFilterPerim = new JPanel();
		panelFilter.add(pnlFilterPerim, "filterHosp");
		GridBagLayout gbl_pnlFilterPerim = new GridBagLayout();
		gbl_pnlFilterPerim.columnWidths = new int[]{100, 0, 0};
		gbl_pnlFilterPerim.rowHeights = new int[]{30, 0};
		gbl_pnlFilterPerim.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_pnlFilterPerim.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		pnlFilterPerim.setLayout(gbl_pnlFilterPerim);
		
		txtFilterPerim = new JTextField();
		txtFilterPerim.setName("fperim");
		GridBagConstraints gbc_txtFilterPerim = new GridBagConstraints();
		gbc_txtFilterPerim.insets = new Insets(5, 5, 0, 5);
		gbc_txtFilterPerim.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFilterPerim.gridx = 0;
		gbc_txtFilterPerim.gridy = 0;
		pnlFilterPerim.add(txtFilterPerim, gbc_txtFilterPerim);
		txtFilterPerim.setToolTipText("Filter perimeters by typing anything here");
		txtFilterPerim.addKeyListener(new FilterListener(DbCtrl));
		txtFilterPerim.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		JButton btnSavePerim = new JButton("Save changes");
		GridBagConstraints gbc_btnSavePerim = new GridBagConstraints();
		gbc_btnSavePerim.gridx = 1;
		gbc_btnSavePerim.gridy = 0;
		pnlFilterPerim.add(btnSavePerim, gbc_btnSavePerim);
		btnSavePerim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DbCtrl.getPerimCtrl().updatePerimeters(frmBeSafeSecurity);
			}
		});
		
		JPanel pnlFilterLoc = new JPanel();
		panelFilter.add(pnlFilterLoc, "filterLoc");
		GridBagLayout gbl_pnlFilterLoc = new GridBagLayout();
		gbl_pnlFilterLoc.columnWidths = new int[]{100, 0, 0};
		gbl_pnlFilterLoc.rowHeights = new int[]{30, 0};
		gbl_pnlFilterLoc.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_pnlFilterLoc.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		pnlFilterLoc.setLayout(gbl_pnlFilterLoc);
		
		txtFilterLoc = new JTextField();
		txtFilterLoc.setName("floc");
		txtFilterLoc.setToolTipText("Filter locations by typing anything here");
		txtFilterLoc.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_txtFilterLoc = new GridBagConstraints();
		gbc_txtFilterLoc.insets = new Insets(5, 5, 0, 5);
		gbc_txtFilterLoc.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFilterLoc.gridx = 0;
		gbc_txtFilterLoc.gridy = 0;
		txtFilterLoc.addKeyListener(new FilterListener(DbCtrl));
		pnlFilterLoc.add(txtFilterLoc, gbc_txtFilterLoc);
		
		JButton btnSaveLoc = new JButton("Save changes");
		btnSaveLoc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DbCtrl.getLocCtrl().updateLocations(frmBeSafeSecurity);
			}
		});
		btnSaveLoc.setName("saveLoc");
		GridBagConstraints gbc_btnSaveLoc = new GridBagConstraints();
		gbc_btnSaveLoc.gridx = 1;
		gbc_btnSaveLoc.gridy = 0;
		pnlFilterLoc.add(btnSaveLoc, gbc_btnSaveLoc);
		
		JPanel pnlFilterClients = new JPanel();
		panelFilter.add(pnlFilterClients, "filterClient");
		GridBagLayout gbl_pnlFilterClients = new GridBagLayout();
		gbl_pnlFilterClients.columnWidths = new int[]{100, 0, 0};
		gbl_pnlFilterClients.rowHeights = new int[]{30, 0};
		gbl_pnlFilterClients.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_pnlFilterClients.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		pnlFilterClients.setLayout(gbl_pnlFilterClients);
		
		txtFilterClients = new JTextField();
		txtFilterClients.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtFilterClients.setToolTipText("Filter patients by typing anything here");
		txtFilterClients.setName("fclient");
		GridBagConstraints gbc_txtFilterClients = new GridBagConstraints();
		gbc_txtFilterClients.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFilterClients.insets = new Insets(5, 5, 0, 5);
		gbc_txtFilterClients.gridx = 0;
		gbc_txtFilterClients.gridy = 0;
		txtFilterClients.addKeyListener(new FilterListener(DbCtrl));
		pnlFilterClients.add(txtFilterClients, gbc_txtFilterClients);
		
		JButton btnSaveClients = new JButton("Save changes");
		btnSaveClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DbCtrl.getClientCtrl().updateClients(frmBeSafeSecurity);
			}
		});
		GridBagConstraints gbc_btnSaveClients = new GridBagConstraints();
		gbc_btnSaveClients.gridx = 1;
		gbc_btnSaveClients.gridy = 0;
		pnlFilterClients.add(btnSaveClients, gbc_btnSaveClients);
		
		tablePanel = new JPanel();
		GridBagConstraints gbc_panelTable = new GridBagConstraints();
		gbc_panelTable.insets = new Insets(10, 15, 6, 15);
		gbc_panelTable.gridwidth = 3;
		gbc_panelTable.fill = GridBagConstraints.BOTH;
		gbc_panelTable.gridx = 0;
		gbc_panelTable.gridy = 2;
		contentPanel.add(tablePanel, gbc_panelTable);
		tablePanel.setLayout(new BorderLayout(0, 0));
		
		JSeparator tableHorizSep = new JSeparator();
		tableHorizSep.setPreferredSize(new Dimension(4, 5));
		tableHorizSep.setForeground(new Color(0, 128, 128));
		GridBagConstraints gbc_tableHorizSep = new GridBagConstraints();
		gbc_tableHorizSep.fill = GridBagConstraints.HORIZONTAL;
		gbc_tableHorizSep.insets = new Insets(0, 0, 5, 0);
		gbc_tableHorizSep.gridwidth = 3;
		gbc_tableHorizSep.gridx = 0;
		gbc_tableHorizSep.gridy = 0;
		contentPanel.add(tableHorizSep, gbc_tableHorizSep);
		
		buttonsPanel = new JPanel();
		frmBeSafeSecurity.getContentPane().add(buttonsPanel, BorderLayout.CENTER);
		buttonsPanel.setLayout(new CardLayout(0, 0));
		
		JPanel perimBtnsPanel = new JPanel();
		buttonsPanel.add(perimBtnsPanel, "perimeters");
		GridBagLayout gbl_perimBtnsPanel = new GridBagLayout();
		gbl_perimBtnsPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_perimBtnsPanel.rowHeights = new int[]{1, 0, 0, 0};
		gbl_perimBtnsPanel.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_perimBtnsPanel.rowWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		perimBtnsPanel.setLayout(gbl_perimBtnsPanel);
		
		JButton btnAddPerim = new JButton("<html><b>(+)</b> Add perimeter</html>");
		btnAddPerim.setName("addPerimeter");
		btnAddPerim.addActionListener(new AddListener(DbCtrl, addPerimFrame, tablePanel));
		GridBagConstraints gbc_btnAddPerim = new GridBagConstraints();
		gbc_btnAddPerim.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAddPerim.insets = new Insets(0, 10, 5, 5);
		gbc_btnAddPerim.gridx = 0;
		gbc_btnAddPerim.gridy = 1;
		perimBtnsPanel.add(btnAddPerim, gbc_btnAddPerim);
		
		JButton btnRmvPerim = new JButton("<html><b>(-)</b> Remove perimeter</html>");
		btnRmvPerim.addActionListener(new DeleteListener(DbCtrl, tablePanel));
		btnRmvPerim.setName("delPerimeter");
		GridBagConstraints gbc_btnRmvPerim = new GridBagConstraints();
		gbc_btnRmvPerim.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRmvPerim.insets = new Insets(0, 5, 5, 5);
		gbc_btnRmvPerim.gridx = 1;
		gbc_btnRmvPerim.gridy = 1;
		perimBtnsPanel.add(btnRmvPerim, gbc_btnRmvPerim);
		
		JButton btnShowLoc = new JButton("Show locations");
		btnShowLoc.addActionListener(new ShowLocationsListener(DbCtrl, tablePanel));
		GridBagConstraints gbc_btnShowLoc = new GridBagConstraints();
		gbc_btnShowLoc.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnShowLoc.insets = new Insets(0, 5, 5, 0);
		gbc_btnShowLoc.gridx = 2;
		gbc_btnShowLoc.gridy = 1;
		perimBtnsPanel.add(btnShowLoc, gbc_btnShowLoc);
		
		JPanel locationBtnsPanel = new JPanel();
		buttonsPanel.add(locationBtnsPanel, "locations");
		GridBagLayout gbl_locationBtnsPanel = new GridBagLayout();
		gbl_locationBtnsPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_locationBtnsPanel.rowHeights = new int[]{1, 0, 0, 0};
		gbl_locationBtnsPanel.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_locationBtnsPanel.rowWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		locationBtnsPanel.setLayout(gbl_locationBtnsPanel);
		
		JButton btnAddLoc = new JButton("<html><b>(+)</b> Add location</html>");
		btnAddLoc.setName("addLocation");
		btnAddLoc.addActionListener(new AddListener(DbCtrl, addLocFrame, tablePanel));
		GridBagConstraints gbc_btnAddLoc = new GridBagConstraints();
		gbc_btnAddLoc.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAddLoc.insets = new Insets(0, 10, 5, 5);
		gbc_btnAddLoc.gridx = 0;
		gbc_btnAddLoc.gridy = 1;
		locationBtnsPanel.add(btnAddLoc, gbc_btnAddLoc);
		
		JButton btnRmvLoc = new JButton("<html><b>(-)</b> Remove location</html>");
		btnRmvLoc.setName("delLocation");
		GridBagConstraints gbc_btnRmvLoc = new GridBagConstraints();
		gbc_btnRmvLoc.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRmvLoc.insets = new Insets(0, 5, 5, 5);
		gbc_btnRmvLoc.gridx = 1;
		gbc_btnRmvLoc.gridy = 1;
		btnRmvLoc.addActionListener(new DeleteListener(DbCtrl, tablePanel));
		locationBtnsPanel.add(btnRmvLoc, gbc_btnRmvLoc);
		
		JButton btnShowAdmins = new JButton("Show current admins");
		btnShowAdmins.addActionListener(new ShowClientsListener(DbCtrl, tablePanel));
		GridBagConstraints gbc_btnShowAdmins = new GridBagConstraints();
		gbc_btnShowAdmins.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnShowAdmins.insets = new Insets(0, 5, 5, 0);
		gbc_btnShowAdmins.gridx = 2;
		gbc_btnShowAdmins.gridy = 1;
		locationBtnsPanel.add(btnShowAdmins, gbc_btnShowAdmins);
		
		JPanel clientsBtnsPanel = new JPanel();
		buttonsPanel.add(clientsBtnsPanel, "clients");
		GridBagLayout gbl_clientsBtnsPanel = new GridBagLayout();
		gbl_clientsBtnsPanel.columnWidths = new int[]{135, 133, 0, 0};
		gbl_clientsBtnsPanel.rowHeights = new int[]{1, 0, 1, 0};
		gbl_clientsBtnsPanel.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_clientsBtnsPanel.rowWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		clientsBtnsPanel.setLayout(gbl_clientsBtnsPanel);
		
		JButton btnAddClient = new JButton("<html><b>(+)</b> Add client</html>");
		btnAddClient.setName("addClient");
		btnAddClient.addActionListener(new AddListener(DbCtrl, addClientFrame, tablePanel));
		GridBagConstraints gbc_btnAddClient = new GridBagConstraints();
		gbc_btnAddClient.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAddClient.insets = new Insets(0, 10, 5, 5);
		gbc_btnAddClient.gridx = 0;
		gbc_btnAddClient.gridy = 1;
		clientsBtnsPanel.add(btnAddClient, gbc_btnAddClient);
		
		JButton btnRmvClient = new JButton("<html><b>(-)</b> Remove client</html>");
		btnRmvClient.addActionListener(new DeleteListener(DbCtrl, tablePanel));
		btnRmvClient.setName("delClient");
		GridBagConstraints gbc_btnRmvClient = new GridBagConstraints();
		gbc_btnRmvClient.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRmvClient.insets = new Insets(0, 5, 5, 5);
		gbc_btnRmvClient.gridx = 1;
		gbc_btnRmvClient.gridy = 1;
		clientsBtnsPanel.add(btnRmvClient, gbc_btnRmvClient);
			
		JComboBox cmboxManage = new JComboBox(manageOptions);
		GridBagConstraints gbc_cmboxManage = new GridBagConstraints();
		gbc_cmboxManage.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmboxManage.insets = new Insets(0, 10, 5, 5);
		gbc_cmboxManage.gridx = 0;
		gbc_cmboxManage.gridy = 2;
		panelChoose.add(cmboxManage, gbc_cmboxManage);
		
		JPanel panelWelcome = new JPanel();
		frmBeSafeSecurity.getContentPane().add(panelWelcome, BorderLayout.NORTH);
		GridBagLayout gbl_panelWelcome = new GridBagLayout();
		gbl_panelWelcome.columnWidths = new int[]{0, 376, 0, 0, 0};
		gbl_panelWelcome.rowHeights = new int[]{0, 0, 0};
		gbl_panelWelcome.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panelWelcome.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panelWelcome.setLayout(gbl_panelWelcome);
		
		JLabel lblNewLabel = new JLabel("SC BE SAFE Security Services SRL");
		lblNewLabel.setForeground(new Color(0, 128, 128));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(10, 0, 10, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 0;
		panelWelcome.add(lblNewLabel, gbc_lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Look and feel:");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(10, 0, 10, 5);
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.gridx = 2;
		gbc_lblNewLabel_2.gridy = 0;
		panelWelcome.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JComboBox cmboxLookAndFeel = new JComboBox(themesNames);
		GridBagConstraints gbc_cmboxLookAndFeel = new GridBagConstraints();
		gbc_cmboxLookAndFeel.anchor = GridBagConstraints.EAST;
		gbc_cmboxLookAndFeel.insets = new Insets(10, 5, 10, 10);
		gbc_cmboxLookAndFeel.gridx = 3;
		gbc_cmboxLookAndFeel.gridy = 0;
		panelWelcome.add(cmboxLookAndFeel, gbc_cmboxLookAndFeel);
		
		JSeparator titleHorizSep = new JSeparator();
		titleHorizSep.setForeground(new Color(0, 128, 128));
		GridBagConstraints gbc_titleHorizSep = new GridBagConstraints();
		gbc_titleHorizSep.fill = GridBagConstraints.HORIZONTAL;
		gbc_titleHorizSep.gridwidth = 4;
		gbc_titleHorizSep.gridx = 0;
		gbc_titleHorizSep.gridy = 1;
		panelWelcome.add(titleHorizSep, gbc_titleHorizSep);
		
		JPanel contactPanel = new JPanel();
		GridBagConstraints gbc_contactPanel = new GridBagConstraints();
		gbc_contactPanel.insets = new Insets(0, 200, 0, 5);
		gbc_contactPanel.fill = GridBagConstraints.BOTH;
		gbc_contactPanel.gridx = 1;
		gbc_contactPanel.gridy = 3;
		contentPanel.add(contactPanel, gbc_contactPanel);
		GridBagLayout gbl_contactPanel = new GridBagLayout();
		gbl_contactPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_contactPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contactPanel.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_contactPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contactPanel.setLayout(gbl_contactPanel);
		
		JLabel lblContact = new JLabel("Contact");
		lblContact.setFont(new Font("Tahoma", Font.BOLD, 12));
		GridBagConstraints gbc_lblContact = new GridBagConstraints();
		gbc_lblContact.insets = new Insets(0, 0, 5, 5);
		gbc_lblContact.gridx = 1;
		gbc_lblContact.gridy = 0;
		contactPanel.add(lblContact, gbc_lblContact);
		
		JLabel lblAddressStrUlmului = new JLabel("<html><B>Address: </B>&nbsp;Str. Ulmului, Nr. 20</html>");
		lblAddressStrUlmului.setFont(new Font("Tahoma", Font.PLAIN, 11));
		GridBagConstraints gbc_lblAddressStrUlmului = new GridBagConstraints();
		gbc_lblAddressStrUlmului.gridx = 1;
		gbc_lblAddressStrUlmului.gridy = 1;
		contactPanel.add(lblAddressStrUlmului, gbc_lblAddressStrUlmului);
		
		JLabel lblPhoneNo = new JLabel("<html><b>Phone no.:</b>&nbsp; 0761 940 715</html>");
		lblPhoneNo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		GridBagConstraints gbc_lblPhoneNo = new GridBagConstraints();
		gbc_lblPhoneNo.insets = new Insets(0, 0, 10, 5);
		gbc_lblPhoneNo.gridx = 1;
		gbc_lblPhoneNo.gridy = 2;
		contactPanel.add(lblPhoneNo, gbc_lblPhoneNo);
		
		frmBeSafeSecurity.setVisible(true);
		
		cmboxManage.addItemListener(new ManageListener(DbCtrl, panelFilter, buttonsPanel, tablePanel));
		cmboxManage.setSelectedItem(userSettings.getProperty("manageOption"));
		
		cmboxLookAndFeel.addItemListener(new ThemeListener(DbCtrl, frmBeSafeSecurity, cmboxManage, tablePanel));
		cmboxLookAndFeel.setSelectedItem(userSettings.getProperty("themeOption"));
		
		frmBeSafeSecurity.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
            	userSettings.setProperty("manageOption", cmboxManage.getSelectedItem().toString());
            	userSettings.setProperty("themeOption", cmboxLookAndFeel.getSelectedItem().toString());
            	try
				{
					userSettings.store(new FileOutputStream("user.properties"), "User Settings");
				} 
            	catch (IOException e1)
				{
					e1.printStackTrace();
				}
                e.getWindow().dispose();
            }
        });
	}
	
	private static void initUserSettings()
	{
		try
		{
			FileInputStream fileIn = new FileInputStream("user.properties");

			userSettings.load(fileIn);
			fileIn.close();

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}

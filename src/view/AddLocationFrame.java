package view;

import java.awt.Font;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import controller.DbController;
import controller.location.AddLocationRegexInputVerifier;
import controller.location.listener.AddLocationListener;

import java.awt.Insets;
import javax.swing.JSeparator;
import javax.swing.JButton;
import java.awt.event.*;
import java.util.BitSet;

import javax.swing.JComboBox;

@SuppressWarnings({"serial", "unchecked", "rawtypes"})
public class AddLocationFrame extends JFrame
{
	private JPanel tablePanel;

	private static DbController DbCtrl;
	private JTextField txtName;
	private JTextField txtAddress;
	private JComboBox cmboxPerim;
	private BitSet textFieldsVerified;
	private String[] perimNames;

	public AddLocationFrame(final DbController _DbCtrl, final JPanel _tablePanel, final String[] _perimNames)
	{
		DbCtrl = _DbCtrl;
		tablePanel = _tablePanel;
		perimNames = _perimNames;
		textFieldsVerified = new BitSet(3);
		cmboxPerim = new JComboBox(perimNames);
		initialize();
	}

	private void initialize()
	{
		this.setTitle("Add new location");
		this.setResizable(false);
		this.setBounds(100, 100, 400, 250);
		this.getContentPane().setLayout(new BorderLayout(0, 0));
		this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
            	DbCtrl.getLocCtrl().showLocations(tablePanel);
            	SwingUtilities.getRoot(tablePanel).setEnabled(true);
                e.getWindow().dispose();
            }
        });

		JPanel titlePanel = new JPanel();
		this.getContentPane().add(titlePanel, BorderLayout.NORTH);
		GridBagLayout gbl_titlePanel = new GridBagLayout();
		gbl_titlePanel.columnWidths = new int[] { 1, 98, 1, 0 };
		gbl_titlePanel.rowHeights = new int[] { 16, 0, 0 };
		gbl_titlePanel.columnWeights = new double[] { 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_titlePanel.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		titlePanel.setLayout(gbl_titlePanel);

		JLabel lblAddNewHospital = new JLabel("Add new location");
		lblAddNewHospital.setForeground(new Color(230, 138, 0));
		lblAddNewHospital.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblAddNewHospital = new GridBagConstraints();
		gbc_lblAddNewHospital.insets = new Insets(10, 0, 10, 5);
		gbc_lblAddNewHospital.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblAddNewHospital.gridx = 1;
		gbc_lblAddNewHospital.gridy = 0;
		titlePanel.add(lblAddNewHospital, gbc_lblAddNewHospital);

		JSeparator separator = new JSeparator();
		separator.setForeground(new Color(230, 138, 0));
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.insets = new Insets(0, 0, 0, 5);
		gbc_separator.gridwidth = 3;
		gbc_separator.fill = GridBagConstraints.HORIZONTAL;
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 1;
		titlePanel.add(separator, gbc_separator);

		JPanel inputPanel = new JPanel();
		this.getContentPane().add(inputPanel, BorderLayout.CENTER);
		GridBagLayout gbl_inputPanel = new GridBagLayout();
		gbl_inputPanel.columnWidths = new int[] { 1, 0, 149, 1, 0 };
		gbl_inputPanel.rowHeights = new int[] { 1, 0, 0, 0, 1, 0 };
		gbl_inputPanel.columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_inputPanel.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		inputPanel.setLayout(gbl_inputPanel);

		JPanel btnsPanel = new JPanel();
		this.getContentPane().add(btnsPanel, BorderLayout.SOUTH);
		GridBagLayout gbl_btnsPanel = new GridBagLayout();
		gbl_btnsPanel.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_btnsPanel.rowHeights = new int[] { 0, 0 };
		gbl_btnsPanel.columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_btnsPanel.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		btnsPanel.setLayout(gbl_btnsPanel);
		
		JButton btnOk = new JButton("OK");
		btnOk.setEnabled(false);
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.insets = new Insets(0, 0, 15, 15);
		gbc_btnOk.gridx = 1;
		gbc_btnOk.gridy = 0;
		btnsPanel.add(btnOk, gbc_btnOk);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DbCtrl.getLocCtrl().showLocations(tablePanel);
				SwingUtilities.getRoot(tablePanel).setEnabled(true);
				dispose();
			}
		});
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 15, 15, 5);
		gbc_btnCancel.gridx = 2;
		gbc_btnCancel.gridy = 0;
		btnsPanel.add(btnCancel, gbc_btnCancel);

		JLabel lblFirstName = new JLabel("Name:");
		lblFirstName.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblFirstName = new GridBagConstraints();
		gbc_lblFirstName.insets = new Insets(5, 5, 5, 5);
		gbc_lblFirstName.gridx = 1;
		gbc_lblFirstName.gridy = 1;
		inputPanel.add(lblFirstName, gbc_lblFirstName);

		txtName = new JTextField();
		txtName.setName("name");
		txtName.setInputVerifier(new AddLocationRegexInputVerifier("^[a-zA-Z0-9][a-zA-Z0-9-. ]{3,}", btnOk, textFieldsVerified));
		txtName.setColumns(10);
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtName.insets = new Insets(5, 5, 5, 5);
		gbc_txtName.gridx = 2;
		gbc_txtName.gridy = 1;
		inputPanel.add(txtName, gbc_txtName);

		JLabel lblLastName = new JLabel("Address:");
		lblLastName.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblLastName = new GridBagConstraints();
		gbc_lblLastName.insets = new Insets(5, 5, 5, 5);
		gbc_lblLastName.gridx = 1;
		gbc_lblLastName.gridy = 2;
		inputPanel.add(lblLastName, gbc_lblLastName);

		txtAddress = new JTextField();
		txtAddress.setName("address");
		txtAddress.setInputVerifier(new AddLocationRegexInputVerifier("^[a-zA-Z0-9][a-zA-Z0-9-,. ]{5,}", btnOk, textFieldsVerified));
		txtAddress.setColumns(10);
		GridBagConstraints gbc_txtAddress = new GridBagConstraints();
		gbc_txtAddress.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAddress.insets = new Insets(5, 5, 5, 5);
		gbc_txtAddress.gridx = 2;
		gbc_txtAddress.gridy = 2;
		inputPanel.add(txtAddress, gbc_txtAddress);

		JLabel lblHospital = new JLabel("Perimeter:");
		lblHospital.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblHospital = new GridBagConstraints();
		gbc_lblHospital.insets = new Insets(5, 5, 5, 5);
		gbc_lblHospital.gridx = 1;
		gbc_lblHospital.gridy = 3;
		inputPanel.add(lblHospital, gbc_lblHospital);
		GridBagConstraints gbc_cmboxPerim = new GridBagConstraints();
		gbc_cmboxPerim.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmboxPerim.insets = new Insets(5, 5, 5, 5);
		gbc_cmboxPerim.gridx = 2;
		gbc_cmboxPerim.gridy = 3;
		inputPanel.add(cmboxPerim, gbc_cmboxPerim);
		
		btnOk.addActionListener(new AddLocationListener(DbCtrl, tablePanel, txtName, txtAddress, cmboxPerim));
	}
}

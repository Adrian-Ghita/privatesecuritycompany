package view;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import common.listener.FilterListener;
import controller.DbController;
import controller.location.LocationController;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ShowLocationsFrame extends JFrame
{
	private JPanel mainTablePanel;
	private JTextField txtSearch;
	private static DbController DbCtrl;
	private static LocationController locCtrl;

	public ShowLocationsFrame(final DbController _DbCtrl, final JPanel _mainTablePanel, final LocationController _locCtrl)
	{
		DbCtrl = _DbCtrl;
		locCtrl = _locCtrl;
		mainTablePanel = _mainTablePanel;
		initialize();
	}

	private void initialize()
	{
		this.setBounds(100, 100, 700, 325);
		this.setResizable(false);
		this.getContentPane().setLayout(new BorderLayout(0, 0));
		this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
            	mainTablePanel.removeAll();
            	SwingUtilities.getRoot(mainTablePanel).setEnabled(true);
            	DbCtrl.getPerimCtrl().showPerimeters(mainTablePanel);
                e.getWindow().dispose();
            }
        });
		
		JPanel contentPanel = new JPanel();
		this.getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{110, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		
		JPanel tablePanel = new JPanel();
		GridBagConstraints gbc_tablePanel = new GridBagConstraints();
		gbc_tablePanel.gridwidth = 2;
		gbc_tablePanel.insets = new Insets(15, 10, 10, 10);
		gbc_tablePanel.fill = GridBagConstraints.BOTH;
		gbc_tablePanel.gridx = 0;
		gbc_tablePanel.gridy = 1;
		contentPanel.add(tablePanel, gbc_tablePanel);
		tablePanel.setLayout(new BorderLayout(0, 0));
		locCtrl.showLocations(tablePanel);
		
		JLabel label = new JLabel("Filter results:");
		label.setVerticalAlignment(SwingConstants.BOTTOM);
		label.setToolTipText("Filter results by typing anything in the search box");
		label.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(12, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		contentPanel.add(label, gbc_label);
		
		JPanel filterPanel = new JPanel();
		filterPanel.setPreferredSize(new Dimension(500, 35));
		GridBagConstraints gbc_filterPanel = new GridBagConstraints();
		gbc_filterPanel.insets = new Insets(5, 0, 5, 0);
		gbc_filterPanel.fill = GridBagConstraints.BOTH;
		gbc_filterPanel.gridx = 1;
		gbc_filterPanel.gridy = 0;
		contentPanel.add(filterPanel, gbc_filterPanel);
		GridBagLayout gbl_filterPanel = new GridBagLayout();
		gbl_filterPanel.columnWidths = new int[]{241, 0, 0, 0};
		gbl_filterPanel.rowHeights = new int[]{30, 0};
		gbl_filterPanel.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_filterPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		filterPanel.setLayout(gbl_filterPanel);
		
		txtSearch = new JTextField();
		txtSearch.setToolTipText("Filter locations by typing anything here");
		txtSearch.setName("fshowloc");
		txtSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_txtSearch = new GridBagConstraints();
		gbc_txtSearch.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSearch.insets = new Insets(10, 5, 0, 10);
		gbc_txtSearch.gridx = 0;
		gbc_txtSearch.gridy = 0;
		txtSearch.addKeyListener(new FilterListener(locCtrl));
		filterPanel.add(txtSearch, gbc_txtSearch);
		
		JButton btnSave = new JButton("Save changes");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				locCtrl.updateLocations((JFrame) SwingUtilities.getRoot(tablePanel));
			}
		});
		btnSave.setName("saveLoc");
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.insets = new Insets(5, 5, 0, 15);
		gbc_btnSave.gridx = 1;
		gbc_btnSave.gridy = 0;
		filterPanel.add(btnSave, gbc_btnSave);
		
		JButton btnRmvLoc = new JButton("<html><b>(-)</b> Remove location</html>");
		btnRmvLoc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				locCtrl.deleteLocation((JFrame) SwingUtilities.getRoot(tablePanel));
				tablePanel.removeAll();
				locCtrl.showLocations(tablePanel);
			}
		});
		btnRmvLoc.setName("delLocation");
		GridBagConstraints gbc_btnRmvLoc = new GridBagConstraints();
		gbc_btnRmvLoc.insets = new Insets(5, 5, 0, 0);
		gbc_btnRmvLoc.gridx = 2;
		gbc_btnRmvLoc.gridy = 0;
		filterPanel.add(btnRmvLoc, gbc_btnRmvLoc);	
	}

}

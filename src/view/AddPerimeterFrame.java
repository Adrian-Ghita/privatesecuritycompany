package view;

import java.awt.Font;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import controller.DbController;
import controller.perimeter.AddPerimeterRegexInputVerifier;
import controller.perimeter.listener.AddPerimeterListener;

import java.awt.Insets;
import javax.swing.JSeparator;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.BitSet;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class AddPerimeterFrame extends JFrame
{
	private JTextField txtName;
	private JTextField txtLocality;
	private JPanel tablePanel;
	private JButton btnOk;
	
	private static DbController DbCtrl;
	private JLabel lblLocality;
	private BitSet textFieldsVerified;

	public AddPerimeterFrame(final DbController _DbCtrl,
							final JPanel _tablePanel)
	{
		DbCtrl = _DbCtrl;
		tablePanel = _tablePanel;
		textFieldsVerified = new BitSet(2);
		initialize();
	}

	private void initialize()
	{
		this.setTitle("Add new perimeter");
		this.setResizable(false);
		this.setBounds(100, 100, 413, 197);
		this.getContentPane().setLayout(new BorderLayout(0, 0));
		this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
            	DbCtrl.getPerimCtrl().showPerimeters(tablePanel);
            	SwingUtilities.getRoot(tablePanel).setEnabled(true);
                e.getWindow().dispose();
            }
        });
		
		JPanel titlePanel = new JPanel();
		this.getContentPane().add(titlePanel, BorderLayout.NORTH);
		GridBagLayout gbl_titlePanel = new GridBagLayout();
		gbl_titlePanel.columnWidths = new int[]{1, 98, 1, 0};
		gbl_titlePanel.rowHeights = new int[]{16, 0, 0};
		gbl_titlePanel.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_titlePanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		titlePanel.setLayout(gbl_titlePanel);
		
		JLabel lblAddNewPerimeter = new JLabel("Add new perimeter");
		lblAddNewPerimeter.setForeground(new Color(230, 138, 0));
		lblAddNewPerimeter.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblAddNewPerimeter = new GridBagConstraints();
		gbc_lblAddNewPerimeter.insets = new Insets(10, 0, 5, 5);
		gbc_lblAddNewPerimeter.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblAddNewPerimeter.gridx = 1;
		gbc_lblAddNewPerimeter.gridy = 0;
		titlePanel.add(lblAddNewPerimeter, gbc_lblAddNewPerimeter);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(new Color(230, 138, 0));
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.insets = new Insets(0, 0, 0, 5);
		gbc_separator.gridwidth = 3;
		gbc_separator.fill = GridBagConstraints.HORIZONTAL;
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 1;
		titlePanel.add(separator, gbc_separator);
		
		JPanel btnsPanel = new JPanel();
		this.getContentPane().add(btnsPanel, BorderLayout.SOUTH);
		GridBagLayout gbl_btnsPanel = new GridBagLayout();
		gbl_btnsPanel.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_btnsPanel.rowHeights = new int[]{0, 0};
		gbl_btnsPanel.columnWeights = new double[]{1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_btnsPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		btnsPanel.setLayout(gbl_btnsPanel);
		
		btnOk = new JButton("OK");
		btnOk.setEnabled(false);
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.insets = new Insets(0, 0, 15, 15);
		gbc_btnOk.gridx = 1;
		gbc_btnOk.gridy = 0;
		btnsPanel.add(btnOk, gbc_btnOk);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DbCtrl.getPerimCtrl().showPerimeters(tablePanel);
				SwingUtilities.getRoot(tablePanel).setEnabled(true);
				dispose();
			}
		});
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 15, 15, 5);
		gbc_btnCancel.gridx = 2;
		gbc_btnCancel.gridy = 0;
		btnsPanel.add(btnCancel, gbc_btnCancel);
		
		JPanel inputPanel = new JPanel();
		this.getContentPane().add(inputPanel, BorderLayout.CENTER);
		GridBagLayout gbl_inputPanel = new GridBagLayout();
		gbl_inputPanel.columnWidths = new int[]{1, 0, 0, 1, 0};
		gbl_inputPanel.rowHeights = new int[]{1, 0, 0, 1, 0};
		gbl_inputPanel.columnWeights = new double[]{1.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_inputPanel.rowWeights = new double[]{1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		inputPanel.setLayout(gbl_inputPanel);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 10, 10);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 1;
		inputPanel.add(lblName, gbc_lblName);
		
		txtName = new JTextField();
		txtName.setName("name");
		txtName.setInputVerifier(new AddPerimeterRegexInputVerifier("^[a-zA-Z0-9][a-zA-Z0-9-. ]{3,}", btnOk, textFieldsVerified));
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtName.insets = new Insets(0, 0, 10, 0);
		gbc_txtName.gridx = 2;
		gbc_txtName.gridy = 1;
		inputPanel.add(txtName, gbc_txtName);
		txtName.setColumns(10);
		
		lblLocality = new JLabel("Locality:");
		lblLocality.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblLocality = new GridBagConstraints();
		gbc_lblLocality.anchor = GridBagConstraints.BASELINE_TRAILING;
		gbc_lblLocality.insets = new Insets(10, 0, 5, 10);
		gbc_lblLocality.gridx = 1;
		gbc_lblLocality.gridy = 2;
		inputPanel.add(lblLocality, gbc_lblLocality);
		
		txtLocality = new JTextField();
		txtLocality.setName("locality");
		txtLocality.setInputVerifier(new AddPerimeterRegexInputVerifier("^[a-zA-Z0-9][a-zA-Z0-9-. ]{3,}", btnOk, textFieldsVerified));
		txtLocality.setColumns(10);
		GridBagConstraints gbc_txtLocality = new GridBagConstraints();
		gbc_txtLocality.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtLocality.insets = new Insets(10, 0, 5, 0);
		gbc_txtLocality.gridx = 2;
		gbc_txtLocality.gridy = 2;
		inputPanel.add(txtLocality, gbc_txtLocality);
		
		btnOk.addActionListener(new AddPerimeterListener(DbCtrl, tablePanel, txtName, txtLocality));
	}
}

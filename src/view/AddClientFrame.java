package view;

import java.awt.Font;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.SqlDateModel;

import common.DateLabelFormatter;
import controller.DbController;
import controller.client.AddClientRegexInputVerifier;
import controller.client.listener.AddClientListener;

import java.awt.Insets;
import javax.swing.JSeparator;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.BitSet;
import java.util.Properties;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

@SuppressWarnings("serial")
public class AddClientFrame extends JFrame
{
	private JPanel tablePanel;

	private static DbController DbCtrl;
	private JTextField txtCNP;
	private JTextField txtFN;
	private JTextField txtLN;
	private JTextField txtAddress;

	private String[] locNames;
	private JTextField txtRank;
	private JTextField txtPhone;
	
	private BitSet textFieldsVerified;

	public AddClientFrame(final DbController _DbCtrl, final JPanel _tablePanel, final String[] _locNames)
	{
		DbCtrl = _DbCtrl;
		tablePanel = _tablePanel;
		locNames = _locNames;
		textFieldsVerified = new BitSet(6);
		initialize();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize()
	{
		this.setTitle("Add new patient");
		this.setResizable(false);
		this.setBounds(100, 100, 413, 375);
		this.getContentPane().setLayout(new BorderLayout(0, 0));
		this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
            	DbCtrl.getClientCtrl().showClients(tablePanel);
            	SwingUtilities.getRoot(tablePanel).setEnabled(true);
                e.getWindow().dispose();
            }
        });

		JPanel titlePanel = new JPanel();
		this.getContentPane().add(titlePanel, BorderLayout.NORTH);
		GridBagLayout gbl_titlePanel = new GridBagLayout();
		gbl_titlePanel.columnWidths = new int[] { 1, 98, 1, 0 };
		gbl_titlePanel.rowHeights = new int[] { 16, 0, 0 };
		gbl_titlePanel.columnWeights = new double[] { 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_titlePanel.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		titlePanel.setLayout(gbl_titlePanel);

		JLabel lblAddNewClient = new JLabel("Add new client");
		lblAddNewClient.setForeground(new Color(230, 138, 0));
		lblAddNewClient.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblAddNewClient = new GridBagConstraints();
		gbc_lblAddNewClient.insets = new Insets(10, 0, 5, 5);
		gbc_lblAddNewClient.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblAddNewClient.gridx = 1;
		gbc_lblAddNewClient.gridy = 0;
		titlePanel.add(lblAddNewClient, gbc_lblAddNewClient);

		JSeparator separator = new JSeparator();
		separator.setForeground(new Color(230, 138, 0));
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.insets = new Insets(0, 0, 0, 5);
		gbc_separator.gridwidth = 3;
		gbc_separator.fill = GridBagConstraints.HORIZONTAL;
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 1;
		titlePanel.add(separator, gbc_separator);

		JPanel inputPanel = new JPanel();
		this.getContentPane().add(inputPanel, BorderLayout.CENTER);
		GridBagLayout gbl_inputPanel = new GridBagLayout();
		gbl_inputPanel.columnWidths = new int[] { 1, 0, 149, 1, 0 };
		gbl_inputPanel.rowHeights = new int[] { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 };
		gbl_inputPanel.columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_inputPanel.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		inputPanel.setLayout(gbl_inputPanel);

		JPanel btnsPanel = new JPanel();
		this.getContentPane().add(btnsPanel, BorderLayout.SOUTH);
		GridBagLayout gbl_btnsPanel = new GridBagLayout();
		gbl_btnsPanel.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_btnsPanel.rowHeights = new int[] { 0, 0 };
		gbl_btnsPanel.columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_btnsPanel.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		btnsPanel.setLayout(gbl_btnsPanel);
		
		JButton btnOk = new JButton("OK");
		btnOk.setEnabled(false);
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.insets = new Insets(0, 0, 15, 15);
		gbc_btnOk.gridx = 1;
		gbc_btnOk.gridy = 0;
		btnsPanel.add(btnOk, gbc_btnOk);

		JLabel lblCNP = new JLabel("CNP:");
		lblCNP.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblCNP = new GridBagConstraints();
		gbc_lblCNP.insets = new Insets(0, 0, 5, 5);
		gbc_lblCNP.gridx = 1;
		gbc_lblCNP.gridy = 1;
		inputPanel.add(lblCNP, gbc_lblCNP);

		txtCNP = new JTextField();
		txtCNP.setName("cnp");
		txtCNP.setInputVerifier(new AddClientRegexInputVerifier("[1-2]\\d{2}(0[1-9]|1[0-2])(0[1-9]|[12]\\d|3[01])(0[1-9]|[1-4]\\d|5[0-2]|99)\\d{4}",
				btnOk, textFieldsVerified));
		txtCNP.setColumns(10);
		GridBagConstraints gbc_txtCNP = new GridBagConstraints();
		gbc_txtCNP.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCNP.insets = new Insets(0, 0, 5, 5);
		gbc_txtCNP.gridx = 2;
		gbc_txtCNP.gridy = 1;
		inputPanel.add(txtCNP, gbc_txtCNP);

		JLabel lblFirstName = new JLabel("First name:");
		lblFirstName.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblFirstName = new GridBagConstraints();
		gbc_lblFirstName.insets = new Insets(0, 0, 5, 5);
		gbc_lblFirstName.gridx = 1;
		gbc_lblFirstName.gridy = 2;
		inputPanel.add(lblFirstName, gbc_lblFirstName);

		txtFN = new JTextField();
		txtFN.setName("fname");
		txtFN.setInputVerifier(new AddClientRegexInputVerifier("^[a-zA-Z][a-zA-Z- ]{3,}", btnOk, textFieldsVerified));
		txtFN.setColumns(10);
		GridBagConstraints gbc_txtFN = new GridBagConstraints();
		gbc_txtFN.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFN.insets = new Insets(0, 0, 5, 5);
		gbc_txtFN.gridx = 2;
		gbc_txtFN.gridy = 2;
		inputPanel.add(txtFN, gbc_txtFN);

		JLabel lblLastName = new JLabel("Last name:");
		lblLastName.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblLastName = new GridBagConstraints();
		gbc_lblLastName.insets = new Insets(0, 0, 5, 5);
		gbc_lblLastName.gridx = 1;
		gbc_lblLastName.gridy = 3;
		inputPanel.add(lblLastName, gbc_lblLastName);

		txtLN = new JTextField();
		txtLN.setName("lname");
		txtLN.setInputVerifier(new AddClientRegexInputVerifier("^[a-zA-Z][a-zA-Z- ]{3,}", btnOk, textFieldsVerified));
		txtLN.setColumns(10);
		GridBagConstraints gbc_txtLN = new GridBagConstraints();
		gbc_txtLN.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtLN.insets = new Insets(0, 0, 5, 5);
		gbc_txtLN.gridx = 2;
		gbc_txtLN.gridy = 3;
		inputPanel.add(txtLN, gbc_txtLN);

		JLabel lblBirthday = new JLabel("Birthday:");
		lblBirthday.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblBirthday = new GridBagConstraints();
		gbc_lblBirthday.insets = new Insets(0, 0, 5, 5);
		gbc_lblBirthday.gridx = 1;
		gbc_lblBirthday.gridy = 4;
		inputPanel.add(lblBirthday, gbc_lblBirthday);

		SqlDateModel model = new SqlDateModel();
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
		JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		GridBagConstraints gbc_txtBirth = new GridBagConstraints();
		gbc_txtBirth.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtBirth.insets = new Insets(0, 0, 5, 5);
		gbc_txtBirth.gridx = 2;
		gbc_txtBirth.gridy = 4;
		inputPanel.add(datePicker, gbc_txtBirth);

		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblAddress = new GridBagConstraints();
		gbc_lblAddress.insets = new Insets(0, 0, 5, 5);
		gbc_lblAddress.gridx = 1;
		gbc_lblAddress.gridy = 5;
		inputPanel.add(lblAddress, gbc_lblAddress);

		txtAddress = new JTextField();
		txtAddress.setName("address");
		txtAddress.setInputVerifier(new AddClientRegexInputVerifier("^[a-zA-Z0-9][a-zA-Z0-9-,. ]{6,}", btnOk, textFieldsVerified));
		txtAddress.setColumns(10);
		GridBagConstraints gbc_txtAddress = new GridBagConstraints();
		gbc_txtAddress.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAddress.insets = new Insets(0, 0, 5, 5);
		gbc_txtAddress.gridx = 2;
		gbc_txtAddress.gridy = 5;
		inputPanel.add(txtAddress, gbc_txtAddress);

		JLabel lblPhone = new JLabel("Phone no.:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblPhone = new GridBagConstraints();
		gbc_lblPhone.insets = new Insets(0, 0, 5, 5);
		gbc_lblPhone.gridx = 1;
		gbc_lblPhone.gridy = 6;
		inputPanel.add(lblPhone, gbc_lblPhone);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DbCtrl.getClientCtrl().showClients(tablePanel);
				SwingUtilities.getRoot(tablePanel).setEnabled(true);
				dispose();
			}
		});
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 15, 15, 5);
		gbc_btnCancel.gridx = 2;
		gbc_btnCancel.gridy = 0;
		btnsPanel.add(btnCancel, gbc_btnCancel);
		
		txtPhone = new JTextField();
		txtPhone.setName("phone");
		txtPhone.setInputVerifier(new AddClientRegexInputVerifier("\\b0{1}7{1}[2-9]{1}\\d{1}[-. ]?\\d{3}[-. ]?\\d{3}\\b", btnOk, textFieldsVerified));
		txtPhone.setColumns(10);
		GridBagConstraints gbc_txtPhone = new GridBagConstraints();
		gbc_txtPhone.insets = new Insets(0, 0, 5, 5);
		gbc_txtPhone.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPhone.gridx = 2;
		gbc_txtPhone.gridy = 6;
		inputPanel.add(txtPhone, gbc_txtPhone);
		
		JLabel lblRank = new JLabel("Rank:");
		lblRank.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblRank = new GridBagConstraints();
		gbc_lblRank.insets = new Insets(0, 0, 5, 5);
		gbc_lblRank.gridx = 1;
		gbc_lblRank.gridy = 7;
		inputPanel.add(lblRank, gbc_lblRank);
		
		txtRank = new JTextField();
		txtRank.setName("rank");
		txtRank.setInputVerifier(new AddClientRegexInputVerifier("^[a-zA-Z][a-zA-Z- ]{2,}", btnOk, textFieldsVerified));
		txtRank.setColumns(10);
		GridBagConstraints gbc_txtRank = new GridBagConstraints();
		gbc_txtRank.insets = new Insets(0, 0, 5, 5);
		gbc_txtRank.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRank.gridx = 2;
		gbc_txtRank.gridy = 7;
		inputPanel.add(txtRank, gbc_txtRank);
		
		JLabel lblLocation = new JLabel("Location:");
		lblLocation.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_lblLocation = new GridBagConstraints();
		gbc_lblLocation.insets = new Insets(0, 0, 5, 5);
		gbc_lblLocation.gridx = 1;
		gbc_lblLocation.gridy = 8;
		inputPanel.add(lblLocation, gbc_lblLocation);
		
		JComboBox cmboxLocation = new JComboBox(locNames);
		GridBagConstraints gbc_cmboxLocation = new GridBagConstraints();
		gbc_cmboxLocation.insets = new Insets(0, 0, 5, 5);
		gbc_cmboxLocation.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmboxLocation.gridx = 2;
		gbc_cmboxLocation.gridy = 8;
		inputPanel.add(cmboxLocation, gbc_cmboxLocation);
		
		btnOk.addActionListener(new AddClientListener(DbCtrl, tablePanel, txtCNP, txtFN, txtLN, 
									datePicker, txtAddress, txtPhone, txtRank, cmboxLocation));
	}
}
